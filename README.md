# STM32F1x系列模板

开发平台：KEIL MDK5

测试MCU：STM32F103C8T6

*注意：代码里面的#include <main.h>中的main.h头文件是头文件集合(即把使用到的头文件全写进main.h一个文件内，这样只需要引入它就引入了全部头文件)，可在projectTemplate/projectTemplate/User文件内查看。*

文件夹描述：

module_Template：模块模板

project_Template：工程模板

system_Template：旧版底层驱动模板

**system_driver_new：新版底层驱动模板**

单片机可复用、可通用组件(学习为主)：[mcu_reuse_development_module: 单片机可复用、可通用开发组件，内含：xymodem，bootloader，MODBUS主从机管理(支持RTU和ASCII)，内存管理(小内存管理算法)，串口管理，消息队列，环形缓冲区，类线程间同步方法(信号量、互斥锁、事件集)，软件定时器，命令交互客户端(类命令行)，软件非阻塞延时，数据转换，IO输入输出操作等。 (gitee.com)](https://gitee.com/ashuai0110/mcu_reuse_development_module)

| 模块模板                 | 旧版底层驱动模板 | 新版底层驱动模板 |
| ------------------------ | ---------------- | ---------------- |
| ADXL345三轴传感器        | ADC              | adc              |
| ASR_CI1122语音识别       | CAN              | adc_dma          |
| BY9301语音播报模块       | DELAY            | dac              |
| DHT11温湿度模块          | EXTI             | flash            |
| DS18B20温度传感器        | FLASH            | gpio             |
| DS1302时钟模块           | GPIO             | iwdg             |
| FIFO缓冲区1              | IIC              | nvic             |
| FIFO缓冲区2              | IICs             | pwm_dma          |
| MEMS数字气体传感器(单个) | NVIC             | rcc              |
| MEMS数字气体传感器(多个) | Pvd              | tim              |
| MLX90614红外测温         | RCC              | usart            |
| MPU6050三轴传感器        | SPI              |                  |
| MultiButton按键驱动      | TIM              |                  |
| OLED屏幕0.96             | USART            |                  |
| OneNET                   | WDOG             |                  |
| PM2.5检测模块            | PWM              |                  |
| RC522-RFID模块           |                  |                  |
| SG90舵机                 |                  |                  |
| SIM900A短信模块          |                  |                  |
| SR04超声波               |                  |                  |
| USART&TIM                |                  |                  |
| WIFIGetAPI               |                  |                  |
| 单向链表                 |                  |                  |
| 智能健康检测模块         |                  |                  |
| 读AB相霍尔编码           |                  |                  |
| HX711称重模块            |                  |                  |
