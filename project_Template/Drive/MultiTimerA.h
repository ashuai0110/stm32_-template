/**
  ******************************************************************************
  * @file    MultiTimerA.h
  * @author  woshiashuai
  * @version V1.0
  * @date    2023-05-11
  * @brief   软件定时器-数组版
  *
  ******************************************************************************
  * @attention
  *
  * 版权声明:内容为编者原创,遵循CC4.0BY-SA版权协议,转载请附上出处链接和本声明
  * 出处链接:https://gitee.com/woshiashuai/mcu_development_module.git
  *
  ******************************************************************************
  */

#ifndef __MULTITIMERA_H
#define __MULTITIMERA_H

#ifdef __cplusplus
extern "C" {
#endif

/* 包含头文件-----------------------------------------------------------------*/
/* 宏定义---------------------------------------------------------------------*/
#define TIMER_A_STOP     (0u) // 不运行
#define TIMER_A_START    (1u) // 运行
// 任务停止运行
#define timerA_stop(id)   (timerA_setRun(id, TIMER_A_STOP))
// 任务开始运行
#define timerA_start(id)  (timerA_setRun(id, TIMER_A_START))
// 任务立即执行
#define timerA_work(id)   { timerA_setRun(id, TIMER_A_START); \
                            timerA_setCurTicks(id, timerA_getTargetTicks(id)); }
/* 类型定义-------------------------------------------------------------------*/
/* 全局变量-------------------------------------------------------------------*/
/* 函数原型-------------------------------------------------------------------*/
unsigned char timerA_create(unsigned char id, unsigned int targetTicks, void(* callback)(void), unsigned char run);
void timerA_delete(unsigned char id);

void timerA_loop(void);
void timerA_timer(void);

void timerA_setCurTicks(unsigned char id, unsigned int curTicks);
unsigned int timerA_getCurTicks(unsigned char id);
void timerA_setTargetTicks(unsigned char id, unsigned int targetTicks);
unsigned int timerA_getTargetTicks(unsigned char id);
void timerA_setRun(unsigned char id, unsigned char run);
unsigned char timerA_getRun(unsigned char id);

#ifdef __cplusplus
}
#endif

#endif /* MultiTimerA.h */
