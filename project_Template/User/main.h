#ifndef __MAIN_H
#define __MAIN_H

// ST lib
#include "stm32f10x.h"
// C lib
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
// system
#include "RCC.h"
#include "DELAY.h"
#include "TIM.h"
#include "USART.h"
#include "GPIO.h"
// drive
#include "MultiTimerA.h"
// user
#include "handle.h"


#endif


