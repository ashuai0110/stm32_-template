#include "main.h"

AllFlagStruct AllFlag = {

};

#define TIMER_PERIOD_MS    10

static void ledBlinkTask(void);

/**
  * @brief  系统初始化
  * @param  None
  * @retval None
  */
void SYSTEM_Init(void)
{
	GPIOClass.Init();
	TIMClass.TIM2_Init(100-1, 7200-1); // 10ms
#ifdef DEBUG_printf
	USARTClass.USART1_Init(115200);
	printf("begin\n");
#endif
	
	timerA_create(0, 500/TIMER_PERIOD_MS, ledBlinkTask, TIMER_A_START);
}
/**
  * @brief  任务处理
  * @param  None
  * @retval None
  */
void TASK_Schedule(void)
{
	timerA_loop();
}
/**
  * @brief  运行指示灯闪烁任务
  * @param  None
  * @retval None
  */
static void ledBlinkTask(void)
{
	GPIOClass.Toggle(LED_Run);
}
/**
  * @brief  TIM2中断函数
  * @param  None
  * @retval None
  */
void TIM2_IRQHandler(void)
{
	if(TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET) 
	{	
		timerA_timer();
		TIM_ClearITPendingBit(TIM2, TIM_FLAG_Update);
	}	
}
