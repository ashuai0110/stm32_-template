#ifndef __GPIO_AS_H
#define	__GPIO_AS_H

#include "main.h"

/*
PA15,PB3,PB4,PB5是JTAG引脚,作为通用IO时需调用引脚重映射函数失能JTAG
GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
PC13,PC14,PC15非必要不使用
*/

typedef struct {
	void (* Init)(void);
	void (* Set)(uint8_t who);
	void (* Reset)(uint8_t who);
	void (* Toggle)(uint8_t who);
	uint8_t (* Read)(uint8_t who);
} GPIOClassStruct;

typedef enum {
	LED_Run = 0,
	LED1,
	LED2,
	LED3
} GPIOxEnum;

extern GPIOClassStruct GPIOClass;

#endif
