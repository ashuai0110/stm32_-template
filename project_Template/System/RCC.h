#ifndef __RCC_AS_H_
#define __RCC_AS_H_

#include "main.h"

typedef struct {
	void (* Set36MHz)(void);
	void (* Set64MHz)(void);
	void (* Set72MHz)(void);
} RCCClassStruct;

extern RCCClassStruct RCCClass;

#endif
