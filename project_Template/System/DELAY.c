#include "main.h"

static void DelayUs(volatile uint32_t us);
static void DelayMs(volatile uint32_t ms);
	
DELAYClassStruct DELAYClass = {
	.DelayMs = DelayMs,
	.DelayUs = DelayUs
};

/**
  * @brief  微秒级延时
	* @param  us: 延时时间(微妙)
  * @retval None
  */
static void DelayUs(volatile uint32_t us)
{
	uint32_t i;
	SysTick_Config(SystemCoreClock/1000000);
	
	for(i=0;i<us;i++)
	{
		// 当计数器的值减小到0的时候，CRTL寄存器的位16会置1	
		while( !((SysTick->CTRL)&(1<<16)) );
	}
	// 关闭SysTick定时器
	SysTick->CTRL &=~SysTick_CTRL_ENABLE_Msk;
}
/**
  * @brief  毫秒级延时
	* @param  ms: 延时时间(毫妙)
  * @retval None
  */
static void DelayMs(volatile uint32_t ms)
{
	uint32_t i;	
	SysTick_Config(SystemCoreClock/1000);
	
	for(i=0;i<ms;i++)
	{
		// 当计数器的值减小到0的时候，CRTL寄存器的位16会置1
		// 当置1时，读取该位会清0
		while( !((SysTick->CTRL)&(1<<16)) );
	}
	// 关闭SysTick定时器
	SysTick->CTRL &=~ SysTick_CTRL_ENABLE_Msk;
}
