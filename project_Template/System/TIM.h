#ifndef __TIM_AS_H
#define __TIM_AS_H

#include "main.h"

typedef struct {
	void (* TIM1_Init)(uint16_t arr, uint16_t psc);
	void (* TIM2_Init)(uint16_t arr, uint16_t psc);
	void (* TIM3_Init)(uint16_t arr, uint16_t psc);
} TIMClassStruct;

extern TIMClassStruct TIMClass;

#endif
