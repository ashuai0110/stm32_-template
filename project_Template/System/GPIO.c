#include "main.h"

static void GPIO_Config(void);
static void GPIO_Set(uint8_t who);
static void GPIO_Reset(uint8_t who);
static void GPIO_Toggle(uint8_t who);
static uint8_t GPIO_Get(uint8_t who);

GPIOClassStruct GPIOClass = {
	.Init = GPIO_Config,
	.Set = GPIO_Set,
	.Reset = GPIO_Reset,
	.Toggle = GPIO_Toggle,
	.Read = GPIO_Get
};

// LEDRun
#define LEDRun_GPIO_PORT    			GPIOB			              /* GPIO端口 */
#define LEDRun_GPIO_CLK 	    		RCC_APB2Periph_GPIOB		/* GPIO端口时钟 */
#define LEDRun_GPIO_PIN						GPIO_Pin_8			        /* 连接到SCL时钟线的GPIO */
#define LEDRun_GPIO_TOGGLE		 		LEDRun_GPIO_PORT->BSRR = ((LEDRun_GPIO_PORT->ODR & LEDRun_GPIO_PIN) << 16) | (~LEDRun_GPIO_PORT->ODR & LEDRun_GPIO_PIN)
#define LEDRun_GPIO_SET			   		LEDRun_GPIO_PORT->BSRR = (uint32_t)LEDRun_GPIO_PIN // 置1
#define LEDRun_GPIO_RESET		   		LEDRun_GPIO_PORT->BSRR = (uint32_t)LEDRun_GPIO_PIN<<16 // 置0
#define LEDRun_GPIO_READ					((LEDRun_GPIO_PORT->IDR & LEDRun_GPIO_PIN) != 0)
// LED1
#define LED1_GPIO_PORT    				GPIOA			              /* GPIO端口 */
#define LED1_GPIO_CLK 	    			RCC_APB2Periph_GPIOA		/* GPIO端口时钟 */
#define LED1_GPIO_PIN							GPIO_Pin_6			        /* 连接到SCL时钟线的GPIO */
#define LED1_GPIO_TOGGLE		 			LED1_GPIO_PORT->BSRR = ((LED1_GPIO_PORT->ODR & LED1_GPIO_PIN) << 16) | (~LED1_GPIO_PORT->ODR & LED1_GPIO_PIN)
#define LED1_GPIO_SET			   			LED1_GPIO_PORT->BSRR = (uint32_t)LED1_GPIO_PIN // 置1
#define LED1_GPIO_RESET		   			LED1_GPIO_PORT->BSRR = (uint32_t)LED1_GPIO_PIN<<16 // 置0
#define LED1_GPIO_READ						((LED1_GPIO_PORT->IDR & LED1_GPIO_PIN) != 0)
// LED2
#define LED2_GPIO_PORT    				GPIOA			              /* GPIO端口 */
#define LED2_GPIO_CLK 	    			RCC_APB2Periph_GPIOA		/* GPIO端口时钟 */
#define LED2_GPIO_PIN							GPIO_Pin_5			        /* 连接到SCL时钟线的GPIO */
#define LED2_GPIO_TOGGLE		 			LED2_GPIO_PORT->BSRR = ((LED2_GPIO_PORT->ODR & LED2_GPIO_PIN) << 16) | (~LED2_GPIO_PORT->ODR & LED2_GPIO_PIN)
#define LED2_GPIO_SET			   			LED2_GPIO_PORT->BSRR = (uint32_t)LED2_GPIO_PIN // 置1
#define LED2_GPIO_RESET		   			LED2_GPIO_PORT->BSRR = (uint32_t)LED2_GPIO_PIN<<16 // 置0
#define LED2_GPIO_READ						((LED2_GPIO_PORT->IDR & LED2_GPIO_PIN) != 0)
// LED3
#define LED3_GPIO_PORT    				GPIOA			              /* GPIO端口 */
#define LED3_GPIO_CLK 	    			RCC_APB2Periph_GPIOA		/* GPIO端口时钟 */
#define LED3_GPIO_PIN							GPIO_Pin_4			        /* 连接到SCL时钟线的GPIO */
#define LED3_GPIO_TOGGLE		 			LED3_GPIO_PORT->BSRR = ((LED3_GPIO_PORT->ODR & LED3_GPIO_PIN) << 16) | (~LED3_GPIO_PORT->ODR & LED3_GPIO_PIN)
#define LED3_GPIO_SET			   			LED3_GPIO_PORT->BSRR = (uint32_t)LED3_GPIO_PIN // 置1
#define LED3_GPIO_RESET		   			LED3_GPIO_PORT->BSRR = (uint32_t)LED3_GPIO_PIN<<16 // 置0
#define LED3_GPIO_READ						((LED3_GPIO_PORT->IDR & LED3_GPIO_PIN) != 0)

/**
  * @brief  IO配置
  * @param  None
  * @retval None
  */
static void GPIO_Config(void)
{		
	/*定义一个GPIO_InitTypeDef类型的结构体*/
	GPIO_InitTypeDef GPIO_InitStructure;
	
	/*开启LED相关的GPIO外设时钟*/
	RCC_APB2PeriphClockCmd(LEDRun_GPIO_CLK, ENABLE);
	GPIO_InitStructure.GPIO_Pin = LEDRun_GPIO_PIN;	
	/*设置引脚模式为*/
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;   
	/*设置引脚速率*/   
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
	/*调用库函数，初始化GPIO*/
	GPIO_Init(LEDRun_GPIO_PORT, &GPIO_InitStructure);	
	/*关闭*/
	LEDRun_GPIO_SET;
	
	RCC_APB2PeriphClockCmd(LED1_GPIO_CLK, ENABLE);
	GPIO_InitStructure.GPIO_Pin = LED1_GPIO_PIN | LED2_GPIO_PIN | LED3_GPIO_PIN;	
	GPIO_Init(LED1_GPIO_PORT, &GPIO_InitStructure);	
	LED1_GPIO_SET;
	LED2_GPIO_SET;
	LED3_GPIO_SET;
}
/**
  * @brief  IO置高
  * @param  None
  * @retval None
  */
static void GPIO_Set(uint8_t who)
{
	switch(who)
	{
		case LED_Run: LEDRun_GPIO_SET;break;
		case LED1: LED1_GPIO_SET;break;
		case LED2: LED2_GPIO_SET;break;
		case LED3: LED3_GPIO_SET;break;
	}
}
/**
  * @brief  IO置低
  * @param  None
  * @retval None
  */
static void GPIO_Reset(uint8_t who)
{
	switch(who)
	{
		case LED_Run: LEDRun_GPIO_RESET;break;
		case LED1: LED1_GPIO_RESET;break;
		case LED2: LED2_GPIO_RESET;break;
		case LED3: LED3_GPIO_RESET;break;
	}
}
/**
  * @brief  IO翻转
  * @param  None
  * @retval None
  */
static void GPIO_Toggle(uint8_t who)
{
	switch(who)
	{
		case LED_Run: LEDRun_GPIO_TOGGLE;break;
		case LED1: LED1_GPIO_TOGGLE;break;
		case LED2: LED2_GPIO_TOGGLE;break;
		case LED3: LED3_GPIO_TOGGLE;break;
	}
}
/**
  * @brief  IO读取
  * @param  None
  * @retval None
  */
static uint8_t GPIO_Get(uint8_t who)
{
	uint8_t temp = 0;
	switch(who)
	{
		case LED_Run: temp = LEDRun_GPIO_READ;break;
		case LED1: temp = LED1_GPIO_READ;break;
		case LED2: temp = LED2_GPIO_READ;break;
		case LED3: temp = LED3_GPIO_READ;break;
	}
	return temp;
}
