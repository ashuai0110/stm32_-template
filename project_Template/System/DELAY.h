#ifndef __DELAY_H
#define __DELAY_H

#include "main.h"

typedef struct {
	void (* DelayMs)(volatile uint32_t ms);
	void (* DelayUs)(volatile uint32_t us);
} DELAYClassStruct;

extern DELAYClassStruct DELAYClass;

#endif





























