/* 包含头文件-----------------------------------------------------------------*/
#include "usart.h"

/* 私有宏定义-----------------------------------------------------------------*/
/* 串口收发引脚重映射功能开关 0:关闭 1:开启 */
#define USART1_REMAP_EN             (0u) /* 默认引脚PA9PA10 重映射引脚PB6PB7 */
#define USART2_REMAP_EN             (0u) /* 默认引脚PA2PA3 重映射引脚PD5PD6 */
/* 串口3特殊 取值为 0:关闭 1:开启部分重映射 2:开启完全重映射 */
#define USART3_REMAP_EN             (0u) /* 默认引脚PB10PB11 部分重映射引脚PC10PC11 完全重映射引脚PD8PD9 */

/* 私有类型定义---------------------------------------------------------------*/

/* 私有变量-------------------------------------------------------------------*/

/* 全局变量-------------------------------------------------------------------*/

/* 私有函数原型---------------------------------------------------------------*/


/* 在别的地方复现此函数  串口1中断函数
void USART1_IRQHandler(void)
{
    if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
    {
        // user handle write
        USART_ClearITPendingBit(USART1, USART_IT_RXNE);
    }
}
*/
/* 在别的地方复现此函数  串口2中断函数
void USART2_IRQHandler(void)
{
    if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
    {
        // user handle write
        USART_ClearITPendingBit(USART2, USART_IT_RXNE);
    }
}
*/
/* 在别的地方复现此函数  串口3中断函数
void USART3_IRQHandler(void)
{
    if(USART_GetITStatus(USART3, USART_IT_RXNE) != RESET)
    {
        // user handle write
        USART_ClearITPendingBit(USART3, USART_IT_RXNE);
    }
}
*/

/**
  * @brief  USART1初始化
  * @param  baud : 串口波特率
  * @retval None
  */
void usart1_config(uint32_t baud)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE); /* 打开串口时钟 */
#if (USART1_REMAP_EN)
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE); /* 打开GPIO时钟 */
    GPIO_PinRemapConfig(GPIO_Remap_USART1, ENABLE); /* 开启重映射 */
    
    /* USART Tx的GPIO配置 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6; /* 设置引脚号 */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; /* 设置引脚模式 复用推挽 */
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; /* 设置引脚速度 */
    GPIO_Init(GPIOB, &GPIO_InitStructure); /* 初始化GPIO */
    /* USART Rx的GPIO配置 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7; /* 设置引脚号 */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING; /* 设置引脚模式 浮空输入 */
    GPIO_Init(GPIOB, &GPIO_InitStructure); /* 初始化GPIO */
#else
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE); /* 打开GPIO时钟 */
    
    /* USART Tx的GPIO配置 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9; /* 设置引脚号 */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; /* 设置引脚模式 复用推挽 */
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; /* 设置引脚速度 */
    GPIO_Init(GPIOA, &GPIO_InitStructure); /* 初始化GPIO */
    /* USART Rx的GPIO配置 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10; /* 设置引脚号 */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING; /* 设置引脚模式 浮空输入 */
    GPIO_Init(GPIOA, &GPIO_InitStructure); /* 初始化GPIO */
#endif

    USART_DeInit(USART1);

    USART_InitStructure.USART_BaudRate = baud; /* 波特率 */
    USART_InitStructure.USART_WordLength = USART_WordLength_8b; /* 数据长度 8bit */
    USART_InitStructure.USART_StopBits = USART_StopBits_1; /* 停止位 1bit */
    USART_InitStructure.USART_Parity = USART_Parity_No; /* 校验位 无 */
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None; /* 硬件流控 无 */
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx; /* 工作模式 接收和发送 */
    USART_Init(USART1, &USART_InitStructure); /* 初始化串口 */

    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE); /* 使能接收中断 */

    USART_Cmd(USART1, ENABLE); /* 开启串口 */
}

/**
  * @brief  USART2初始化
  * @param  baud : 串口波特率
  * @retval None
  */
void usart2_config(uint32_t baud)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE); /* 打开串口时钟 */
#if (USART2_REMAP_EN)
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD | RCC_APB2Periph_AFIO, ENABLE); /* 打开GPIO时钟 */
    GPIO_PinRemapConfig(GPIO_Remap_USART2, ENABLE); /* 开启重映射 */
    
    /* USART Tx的GPIO配置 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5; /* 设置引脚号 */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; /* 设置引脚模式 复用推挽 */
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; /* 设置引脚速度 */
    GPIO_Init(GPIOB, &GPIO_InitStructure); /* 初始化GPIO */
    /* USART Rx的GPIO配置 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6; /* 设置引脚号 */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING; /* 设置引脚模式 浮空输入 */
    GPIO_Init(GPIOD, &GPIO_InitStructure); /* 初始化GPIO */
#else
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE); /* 打开GPIO时钟 */
    
    /* USART Tx的GPIO配置 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2; /* 设置引脚号 */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; /* 设置引脚模式 复用推挽 */
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; /* 设置引脚速度 */
    GPIO_Init(GPIOA, &GPIO_InitStructure); /* 初始化GPIO */
    /* USART Rx的GPIO配置 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3; /* 设置引脚号 */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING; /* 设置引脚模式 浮空输入 */
    GPIO_Init(GPIOA, &GPIO_InitStructure); /* 初始化GPIO */
#endif

    USART_DeInit(USART2);

    USART_InitStructure.USART_BaudRate = baud; /* 波特率 */
    USART_InitStructure.USART_WordLength = USART_WordLength_8b; /* 数据长度 8bit */
    USART_InitStructure.USART_StopBits = USART_StopBits_1; /* 停止位 1bit */
    USART_InitStructure.USART_Parity = USART_Parity_No; /* 校验位 无 */
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None; /* 硬件流控 无 */
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx; /* 工作模式 接收和发送 */
    USART_Init(USART2, &USART_InitStructure); /* 初始化串口 */

    USART_ITConfig(USART2, USART_IT_RXNE, ENABLE); /* 使能接收中断 */

    USART_Cmd(USART2, ENABLE); /* 开启串口 */
}

/**
  * @brief  USART3初始化
  * @param  baud : 串口波特率
  * @retval None
  */
void usart3_config(uint32_t baud)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE); /* 打开串口时钟 */
#if (1 == USART3_REMAP_EN)
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC | RCC_APB2Periph_AFIO, ENABLE); /* 打开GPIO时钟 */
    GPIO_PinRemapConfig(GPIO_PartialRemap_USART3, ENABLE); /* 开启部分重映射 */
    
    /* USART Tx的GPIO配置 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10; /* 设置引脚号 */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; /* 设置引脚模式 复用推挽 */
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; /* 设置引脚速度 */
    GPIO_Init(GPIOC, &GPIO_InitStructure); /* 初始化GPIO */
    /* USART Rx的GPIO配置 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11; /* 设置引脚号 */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING; /* 设置引脚模式 浮空输入 */
    GPIO_Init(GPIOC, &GPIO_InitStructure); /* 初始化GPIO */
#elif (2 == USART3_REMAP_EN)
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD | RCC_APB2Periph_AFIO, ENABLE); /* 打开GPIO时钟 */
    GPIO_PinRemapConfig(GPIO_FullRemap_USART3, ENABLE); /* 开启完全重映射 */
    /* USART Tx的GPIO配置 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8; /* 设置引脚号 */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; /* 设置引脚模式 复用推挽 */
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; /* 设置引脚速度 */
    GPIO_Init(GPIOD, &GPIO_InitStructure); /* 初始化GPIO */
    /* USART Rx的GPIO配置 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9; /* 设置引脚号 */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING; /* 设置引脚模式 浮空输入 */
    GPIO_Init(GPIOD, &GPIO_InitStructure); /* 初始化GPIO */
#else
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE); /* 打开GPIO时钟 */
    
    /* USART Tx的GPIO配置 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10; /* 设置引脚号 */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; /* 设置引脚模式 复用推挽 */
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; /* 设置引脚速度 */
    GPIO_Init(GPIOB, &GPIO_InitStructure); /* 初始化GPIO */
    /* USART Rx的GPIO配置 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11; /* 设置引脚号 */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING; /* 设置引脚模式 浮空输入 */
    GPIO_Init(GPIOB, &GPIO_InitStructure); /* 初始化GPIO */
#endif

    USART_DeInit(USART3);

    USART_InitStructure.USART_BaudRate = baud; /* 波特率 */
    USART_InitStructure.USART_WordLength = USART_WordLength_8b; /* 数据长度 8bit */
    USART_InitStructure.USART_StopBits = USART_StopBits_1; /* 停止位 1bit */
    USART_InitStructure.USART_Parity = USART_Parity_No; /* 校验位 无 */
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None; /* 硬件流控 无 */
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx; /* 工作模式 接收和发送 */
    USART_Init(USART3, &USART_InitStructure); /* 初始化串口 */

    USART_ITConfig(USART3, USART_IT_RXNE, ENABLE); /* 使能接收中断 */

    USART_Cmd(USART3, ENABLE); /* 开启串口 */
}
