#ifndef __DAC_AS_H
#define __DAC_AS_H

#ifdef __cplusplus
extern "C" {
#endif

/* 包含头文件-----------------------------------------------------------------*/
#include "stm32f10x.h"

/* 宏定义---------------------------------------------------------------------*/

#define DAC_VREF                    (3300) /* 正参考电压 mv */
#define DAC_ACCURACY                ((1ul << 12u) - 1) /* 分辨率 */
#define DAC_CAL_VAL(mv)             ((mv) * ADC_ACCURACY / ADC_VREF) /* mv计算为DAC输出值 */

/* dac_output_chx */
#define DAC_OUTPUT_CH1              (0u)
#define DAC_OUTPUT_CH2              (1u)

/* 类型定义-------------------------------------------------------------------*/

/* 全局变量-------------------------------------------------------------------*/

/* 函数原型-------------------------------------------------------------------*/
void dac_ch1_config(void);
void dac_ch2_config(void);

void dac_output(uint8_t dacChx, uint16_t value);

#ifdef __cplusplus
}
#endif

#endif /* dac.h */
