/* 包含头文件-----------------------------------------------------------------*/
#include "adc_dma.h"

/* 私有宏定义-----------------------------------------------------------------*/
/* ADC1输入通道的引脚定义
ADC_IN0 PA0
ADC_IN1 PA1
ADC_IN2 PA2
ADC_IN3 PA3
ADC_IN4 PA4
ADC_IN5 PA5
ADC_IN6 PA6
ADC_IN7 PA7
ADC_IN8 PB0
ADC_IN9 PB1
*/
#define ADC1_DMA1_CH                (DMA1_Channel1) /* 使用DMA1的通道 */
#define ADC1_INPUT_CH_NUM           (1u) /* 开启转换的通道数目 */

/* 私有类型定义---------------------------------------------------------------*/

/* 私有变量-------------------------------------------------------------------*/
static volatile uint16_t adcConvValBuf[10];

/* 全局变量-------------------------------------------------------------------*/

/* 私有函数原型---------------------------------------------------------------*/


/**
  * @brief  ADC1 DMA初始化
  * @note   io初始化部分需用户自行修改
  * @param  None
  * @retval None
  */
void adc1_dma_config(void)
{
    ADC_InitTypeDef ADC_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;
    DMA_InitTypeDef DMA_InitStructure;
    
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE); /* 打开DMA时钟 */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE); /* 打开ADC时钟 */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE); /* 打开GPIO时钟 */

    /* ADC输入引脚的GPIO配置 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1; /* 设置引脚号 */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN; /* 设置引脚模式 模拟输入 */
    GPIO_Init(GPIOA, &GPIO_InitStructure); /* 初始化GPIO */
    
//    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0; /* 设置引脚号 */
//    GPIO_Init(GPIOB, &GPIO_InitStructure); /* 初始化GPIO */
    
    DMA_DeInit(ADC1_DMA1_CH);
    
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&ADC1->DR; /* 外设基地址 */
    DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)&adcConvValBuf[0]; /* 内存基地址 */
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC; /* 数据方向 外设作为数据源 */
    DMA_InitStructure.DMA_BufferSize = ADC1_INPUT_CH_NUM; /* 数据数量 有几个ADC通道需要转换就是几 */
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable; /* 外设基地址不递增 */
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable; /* 内存基地址递增 */
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord; /* 外设数据大小为半字 2byte */
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord; /* 内存数据大小为半字 2byte */
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular; /* 传输模式 循环传输 */
    DMA_InitStructure.DMA_Priority = DMA_Priority_High; /* DMA优先级 高 */
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable; /* 内存到内存禁用 */
    DMA_Init(ADC1_DMA1_CH, &DMA_InitStructure); /* 初始化DMA */
    
    DMA_Cmd(ADC1_DMA1_CH , ENABLE); /* 开启DMA */
    
    ADC_DeInit(ADC1);
    
    ADC_InitStructure.ADC_Mode = ADC_Mode_Independent; /* ADC模式 只用到ADC1时为单模式 */
    ADC_InitStructure.ADC_ScanConvMode = ENABLE; /* 扫描模式 是否从头到尾扫描ADC规则组内所有通道 */
    ADC_InitStructure.ADC_ContinuousConvMode = ENABLE; /* 连续转换模式 转换完组内最后一个通道时是否重头再来 */
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None; /* 外部触发模式 软件触发 */
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right; /* 转换结果对齐方式 右对齐 */
    ADC_InitStructure.ADC_NbrOfChannel = ADC1_INPUT_CH_NUM; /* 规则组内需转换的通道数目 */
    ADC_Init(ADC1, &ADC_InitStructure); /* 初始化ADC */
    
    ADC_DMACmd(ADC1, ENABLE); /* 开启ADC的DMA */
    
    ADC_Cmd(ADC1, ENABLE); /* 开启ADC */
    
    ADC_ResetCalibration(ADC1); // 初始化ADC校准寄存器  
    while(ADC_GetResetCalibrationStatus(ADC1)); // 等待校准寄存器初始化完成
    
    ADC_StartCalibration(ADC1); // ADC开始校准
    while(ADC_GetCalibrationStatus(ADC1)); // 等待校准完成
    
    adc1_dma_start();
}

/**
  * @brief  启动ADC1的DMA转换
  * @param  None
  * @retval None
  */
void adc1_dma_start(void)
{
    /* 设置规则组通道 转换顺序和采样时间 需要转换几个就写几个 */
    ADC_RegularChannelConfig(ADC1, ADC_IN1, 1, ADC_SampleTime_28Cycles5);
//    ADC_RegularChannelConfig(ADC1, ADC_IN2, 2, ADC_SampleTime_55Cycles5);
    
    ADC_SoftwareStartConvCmd(ADC1, ENABLE); /* 使能软件启动转换 */
}

/**
  * @brief  读取ADC1输入通道DMA转换结果
  * @param  index : 根据规则组顺序 例如组中第一个是PA1则索引0是PA1的ADC值,第二个是PA5则索引1是PA5的ADC值
  * @retval 返回转换结果
  */
uint16_t adc1_dma_get_conv_val(uint8_t index)
{
    return adcConvValBuf[index];
}
