#ifndef __GPIO_AS_H
#define __GPIO_AS_H

#ifdef __cplusplus
extern "C" {
#endif

/* 包含头文件-----------------------------------------------------------------*/
#include "stm32f10x.h"

/* 宏定义---------------------------------------------------------------------*/
/*
PA15,PB3,PB4,PB5是JTAG引脚,作为通用IO时需调用引脚重映射函数失能JTAG
GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
PC13,PC14,PC15非必要不使用
*/

/* 类型定义-------------------------------------------------------------------*/
typedef enum {
    LED_RUN,
    TEST_IN,
    BT_RST,
    BT_PWR
} gpiox_enum_t;

/* 全局变量-------------------------------------------------------------------*/

/* 函数原型-------------------------------------------------------------------*/
void gpio_config(void);
void gpio_set(uint8_t gpiox);
void gpio_reset(uint8_t gpiox);
void gpio_toggle(uint8_t gpiox);
uint8_t gpio_read(uint8_t gpiox);

#ifdef __cplusplus
}
#endif

#endif /* gpio.h */
