/* 包含头文件-----------------------------------------------------------------*/
#include "nvic.h"

/* 私有宏定义-----------------------------------------------------------------*/
/* 中断优先级配置开关(缺少的自行补充) 0:关闭 1:开启 */
#define NVIC_TIM1_UP_EN                 (0u) /* 定时器1溢出更新中断 */
#define NVIC_TIM1_CC_EN                 (0u) /* 定时器1比较中断 */
#define NVIC_TIM2_EN                    (0u) /* 定时器2全局中断 */
#define NVIC_TIM3_EN                    (0u) /* 定时器3全局中断 */
#define NVIC_TIM4_EN                    (0u) /* 定时器4全局中断 */

#define NVIC_USART1_EN                  (0u) /* 串口1全局中断 */
#define NVIC_USART2_EN                  (0u) /* 串口2全局中断 */
#define NVIC_USART3_EN                  (0u) /* 串口3全局中断 */
#define NVIC_UART4_EN                   (0u) /* 串口4全局中断 */
#define NVIC_UART5_EN                   (0u) /* 串口5全局中断 */

#define NVIC_DMA1_CH1_EN                (0u) /* DMA1通道1全局中断 */
#define NVIC_DMA1_CH2_EN                (0u) /* DMA1通道2全局中断 */
#define NVIC_DMA1_CH3_EN                (0u) /* DMA1通道3全局中断 */
#define NVIC_DMA1_CH4_EN                (0u) /* DMA1通道4全局中断 */
#define NVIC_DMA1_CH5_EN                (0u) /* DMA1通道5全局中断 */
#define NVIC_DMA1_CH6_EN                (0u) /* DMA1通道6全局中断 */
#define NVIC_DMA1_CH7_EN                (0u) /* DMA1通道7全局中断 */

#define NVIC_EXTI0_EN                   (0u) /* 外部中断0 */
#define NVIC_EXTI1_EN                   (0u) /* 外部中断1 */
#define NVIC_EXTI2_EN                   (0u) /* 外部中断2 */
#define NVIC_EXTI3_EN                   (0u) /* 外部中断3 */
#define NVIC_EXTI4_EN                   (0u) /* 外部中断4 */

/* 私有类型定义---------------------------------------------------------------*/

/* 私有变量-------------------------------------------------------------------*/

/* 全局变量-------------------------------------------------------------------*/

/* 私有函数原型---------------------------------------------------------------*/

/*=====================================================================================
  NVIC_PriorityGroup  | NVIC_IRQChannelPreemptionPriority | NVIC_IRQChannelSubPriority 
=======================================================================================
 NVIC_PriorityGroup_0 |            0    (0 bits)          |        0~15 (4 bits)       
---------------------------------------------------------------------------------------
 NVIC_PriorityGroup_1 |            0~1  (1 bits)          |        0~7  (3 bits)       
---------------------------------------------------------------------------------------
 NVIC_PriorityGroup_2 |            0~3  (2 bits)          |        0~3  (2 bits)       
---------------------------------------------------------------------------------------
 NVIC_PriorityGroup_3 |            0~7  (3 bits)          |        0~1  (1 bits)       
---------------------------------------------------------------------------------------
 NVIC_PriorityGroup_4 |            0~15 (4 bits)          |        0    (0 bits)       
=====================================================================================*/

/**
  * @brief  NVIC初始化
  * @note   未涉及部分用户自行修改
  * @param  None
  * @retval None
  */
void nvic_config(void)
{
    NVIC_InitTypeDef NVIC_InitStructure;
    
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4); /* 设置中断优先级组 */
    
#if (NVIC_TIM1_UP_EN)
    NVIC_InitStructure.NVIC_IRQChannel = TIM1_UP_IRQn; /* 中断通道标志 */
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1; /* 抢占优先级 */
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0; /* 子优先级 */
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; /* 中断通道使能 */
    NVIC_Init(&NVIC_InitStructure); /* 中断优先级初始化 */
#endif
#if (NVIC_TIM1_CC_EN)
    NVIC_InitStructure.NVIC_IRQChannel = TIM1_CC_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif
#if (NVIC_TIM2_EN)
    NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif
#if (NVIC_TIM3_EN)
    NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif
#if (NVIC_TIM4_EN)
    NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 4;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif

#if (NVIC_USART1_EN)
    NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif
#if (NVIC_USART2_EN)
    NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif
#if (NVIC_USART3_EN)
    NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif
#if (NVIC_UART4_EN)
    NVIC_InitStructure.NVIC_IRQChannel = UART4_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 4;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif
#if (NVIC_UART5_EN)
    NVIC_InitStructure.NVIC_IRQChannel = UART5_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 5;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif

#if (NVIC_DMA1_CH1_EN)
    NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif
#if (NVIC_DMA1_CH2_EN)
    NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif
#if (NVIC_DMA1_CH3_EN)
    NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif
#if (NVIC_DMA1_CH4_EN)
    NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel4_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 4;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif
#if (NVIC_DMA1_CH5_EN)
    NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel5_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 5;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif
#if (NVIC_DMA1_CH6_EN)
    NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel6_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 6;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif
#if (NVIC_DMA1_CH7_EN)
    NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel7_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 7;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif

#if (NVIC_EXTI0_EN)
    NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif
#if (NVIC_EXTI1_EN)
    NVIC_InitStructure.NVIC_IRQChannel = EXTI1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif
#if (NVIC_EXTI2_EN)
    NVIC_InitStructure.NVIC_IRQChannel = EXTI2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif
#if (NVIC_EXTI3_EN)
    NVIC_InitStructure.NVIC_IRQChannel = EXTI3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 4;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif
#if (NVIC_EXTI4_EN)
    NVIC_InitStructure.NVIC_IRQChannel = EXTI4_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 5;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif
}

/**
  * @brief  重启单片机
  * @param  None
  * @retval None
  */
void nvic_mcu_reboot(void)
{
    NVIC_SystemReset();
}
