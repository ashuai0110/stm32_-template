#ifndef __RCC_AS_H
#define __RCC_AS_H

#ifdef __cplusplus
extern "C" {
#endif

/* 包含头文件-----------------------------------------------------------------*/
#include "stm32f10x.h"

/* 宏定义---------------------------------------------------------------------*/

/* 类型定义-------------------------------------------------------------------*/

/* 全局变量-------------------------------------------------------------------*/

/* 函数原型-------------------------------------------------------------------*/
uint8_t rcc_system_clock_config(uint8_t sysclk);

void rcc_system_clock_debug(void);

void rcc_delay_ms(uint32_t ms);
void rcc_delay_us(uint32_t us);

#ifdef __cplusplus
}
#endif

#endif /* rcc.h */
