#ifndef __ADC_DMA_AS_H
#define __ADC_DMA_AS_H

#ifdef __cplusplus
extern "C" {
#endif

/* 包含头文件-----------------------------------------------------------------*/
#include "stm32f10x.h"

/* 宏定义---------------------------------------------------------------------*/

#define ADC_VREF                    (3300u) /* 正参考电压mv */
#define ADC_ACCURACY                ((1ul << 12u) - 1u) /* 分辨率 */
#define ADC_CAL_MV(val)             ((val) * ADC_VREF / ADC_ACCURACY) /* ADC结果计算为mv */

/* adc_input_chx */
#define ADC_IN0                     (0u) /* PA0 */
#define ADC_IN1                     (1u) /* PA1 */
#define ADC_IN2                     (2u) /* PA2 */
#define ADC_IN3                     (3u) /* PA3 */
#define ADC_IN4                     (4u) /* PA4 */
#define ADC_IN5                     (5u) /* PA5 */
#define ADC_IN6                     (6u) /* PA6 */
#define ADC_IN7                     (7u) /* PA7 */
#define ADC_IN8                     (8u) /* PB0 */
#define ADC_IN9                     (9u) /* PB1 */

/* 类型定义-------------------------------------------------------------------*/

/* 全局变量-------------------------------------------------------------------*/

/* 函数原型-------------------------------------------------------------------*/
void adc1_dma_config(void);

void adc1_dma_start(void);

uint16_t adc1_dma_get_conv_val(uint8_t chx);

#ifdef __cplusplus
}
#endif

#endif /* adc_dma.h */
