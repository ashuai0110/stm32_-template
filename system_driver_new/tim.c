/* 包含头文件-----------------------------------------------------------------*/
#include "tim.h"

/* 私有宏定义-----------------------------------------------------------------*/

/* 私有类型定义---------------------------------------------------------------*/

/* 私有变量-------------------------------------------------------------------*/

/* 全局变量-------------------------------------------------------------------*/

/* 私有函数原型---------------------------------------------------------------*/


/* 在别的地方复现此函数  TIM1中断函数
void TIM1_UP_IRQHandler(void)
{
    if(TIM_GetITStatus(TIM1, TIM_IT_Update) != RESET) 
    {
        // user handle write
        TIM_ClearITPendingBit(TIM1, TIM_FLAG_Update);
    }
}
*/
/* 在别的地方复现此函数  TIM2中断函数
void TIM2_IRQHandler(void)
{
    if(TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET) 
    {
        // user handle write
        TIM_ClearITPendingBit(TIM2, TIM_FLAG_Update);
    }
}
*/
/* 在别的地方复现此函数  TIM3中断函数
void TIM3_IRQHandler(void)
{
    if(TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET) 
    {
        // user handle write
        TIM_ClearITPendingBit(TIM3, TIM_FLAG_Update);
    }
}
*/

/**
  * @brief  TIM1初始化
  * @note   定时器计数+1的频率Hz = 定时器时钟频率 / 时钟分频系数 / (时钟预分频数 + 1)
  *         定时器溢出周期s = (自动重装载值 + 1) / 定时器计数+1的频率
  * @param  arr : 自动重装载值 0~0xFFFF
  * @param  psc : 时钟预分频数 0~0xFFFF
  * @retval None
  */
void tim1_config(uint16_t arr, uint16_t psc)
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE); /* 开启定时器时钟 */
    
    TIM_DeInit(TIM1);
    
    TIM_TimeBaseStructure.TIM_Period = arr; /* 自动重装载值 */
    TIM_TimeBaseStructure.TIM_Prescaler = psc; /* 时钟预分频数 */
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; /* 时钟分频系数 1分频 */
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; /* 计数模式 向上计数 */
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0; /* 重复计数器值 0 */
    TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure); /* 初始化定时器 */

    TIM_ITConfig(TIM1, TIM_IT_Update, ENABLE); /* 开启定时器溢出更新中断 */

    TIM_Cmd(TIM1, ENABLE); /* 开启定时器 */
}

/**
  * @brief  TIM2初始化
  * @note   定时器计数+1的频率Hz = 定时器时钟频率 / 时钟分频系数 / (时钟预分频数 + 1)
  *         定时器溢出周期s = (自动重装载值 + 1) / 定时器计数+1的频率
  * @param  arr : 自动重装载值 0~0xFFFF
  * @param  psc : 时钟预分频数 0~0xFFFF
  * @retval None
  */
void tim2_config(uint16_t arr, uint16_t psc)
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE); /* 开启定时器时钟 */
    
    TIM_DeInit(TIM2);
    
    TIM_TimeBaseStructure.TIM_Period = arr; /* 自动重装载值 */
    TIM_TimeBaseStructure.TIM_Prescaler = psc; /* 时钟预分频数 */
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; /* 时钟分频系数 1分频 */
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; /* 计数模式 向上计数 */
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0; /* 重复计数器值 0 */
    TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure); /* 初始化定时器 */

    TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE); /* 开启定时器溢出更新中断 */

    TIM_Cmd(TIM2, ENABLE); /* 开启定时器 */
}

/**
  * @brief  TIM3初始化
  * @note   定时器计数+1的频率Hz = 定时器时钟频率 / 时钟分频系数 / (时钟预分频数 + 1)
  *         定时器溢出周期s = (自动重装载值 + 1) / 定时器计数+1的频率
  * @param  arr : 自动重装载值 0~0xFFFF
  * @param  psc : 时钟预分频数 0~0xFFFF
  * @retval None
  */
void tim3_config(uint16_t arr, uint16_t psc)
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE); /* 开启定时器时钟 */
    
    TIM_DeInit(TIM3);
    
    TIM_TimeBaseStructure.TIM_Period = arr; /* 自动重装载值 */
    TIM_TimeBaseStructure.TIM_Prescaler = psc; /* 时钟预分频数 */
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; /* 时钟分频系数 1分频 */
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; /* 计数模式 向上计数 */
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0; /* 重复计数器值 0 */
    TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure); /* 初始化定时器 */

    TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE); /* 开启定时器溢出更新中断 */

    TIM_Cmd(TIM3, ENABLE); /* 开启定时器 */
}
