#ifndef __FLASH_AS_H
#define __FLASH_AS_H

#ifdef __cplusplus
extern "C" {
#endif

/* 包含头文件-----------------------------------------------------------------*/
#include "stm32f10x.h"

/* 宏定义---------------------------------------------------------------------*/

/* 类型定义-------------------------------------------------------------------*/

/* 全局变量-------------------------------------------------------------------*/

/* 函数原型-------------------------------------------------------------------*/
uint8_t flash_erase(uint32_t startAddr, uint32_t bytes);
uint8_t flash_read(uint32_t startAddr, void *pBuf, uint32_t len);
uint8_t flash_write(uint32_t startAddr, void *pBuf, uint32_t len);


#ifdef __cplusplus
}
#endif

#endif /* flash.h */
