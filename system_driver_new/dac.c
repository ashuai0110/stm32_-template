/* 包含头文件-----------------------------------------------------------------*/
#include "dac.h"

/* 私有宏定义-----------------------------------------------------------------*/

/* 私有类型定义---------------------------------------------------------------*/

/* 私有变量-------------------------------------------------------------------*/

/* 全局变量-------------------------------------------------------------------*/

/* 私有函数原型---------------------------------------------------------------*/


/**
  * @brief  DAC通道1初始化
  * @note   PA4引脚输出
  * @param  None
  * @retval None
  */
void dac_ch1_config(void)
{
    DAC_InitTypeDef  DAC_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;
    
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE); /* 开启GPIO外设相关时钟 */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE); /* 开启DAC时钟 */
    
    /* DAC输出引脚的GPIO配置 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4; /* 设置引脚号 */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN; /* 设置引脚模式 模拟输入 */
    GPIO_Init(GPIOA, &GPIO_InitStructure); /* 初始化GPIO */
    
    DAC_InitStructure.DAC_Trigger = DAC_Trigger_Software; /* 软件触发 */
    DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None; /* 不使用波形发生 */
    DAC_InitStructure.DAC_LFSRUnmask_TriangleAmplitude = DAC_LFSRUnmask_Bit0; /* LFSR掩码0 */
    DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Disable; /* 失能输出缓冲 */
    DAC_Init(DAC_Channel_1, &DAC_InitStructure); /* 初始化DAC通道1 */
    
    DAC_Cmd(DAC_Channel_1, ENABLE); /* 开启DAC通道1 */
    /* 默认12bit右对齐输出0 */
    DAC_SetChannel1Data(DAC_Align_12b_R ,0);
    DAC_SoftwareTriggerCmd(DAC_Channel_1, ENABLE);
}

/**
  * @brief  DAC通道2初始化
  * @note   PA5引脚输出
  * @param  None
  * @retval None
  */
void dac_ch2_config(void)
{
    DAC_InitTypeDef  DAC_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;
    
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE); /* 开启GPIO外设相关时钟 */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE); /* 开启DAC时钟 */
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5; /* 设置引脚号 */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN; /* 设置引脚模式 */
    GPIO_Init(GPIOA, &GPIO_InitStructure); /* 初始化GPIO */
    
    DAC_InitStructure.DAC_Trigger = DAC_Trigger_Software; /* 软件触发 */
    DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None; /* 不使用波形发生 */
    DAC_InitStructure.DAC_LFSRUnmask_TriangleAmplitude = DAC_LFSRUnmask_Bit0; /* LFSR掩码0 */
    DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Disable; /* 失能输出缓冲 */
    DAC_Init(DAC_Channel_2, &DAC_InitStructure); /* 初始化DAC通道1 */
    
    DAC_Cmd(DAC_Channel_2, ENABLE); /* 开启DAC通道1 */
    /* 默认12bit右对齐输出0 */
    DAC_SetChannel2Data(DAC_Align_12b_R ,0);
    DAC_SoftwareTriggerCmd(DAC_Channel_2, ENABLE);
}

/**
  * @brief  DAC输出
  * @param  dacChx         : DAC输出通道 @ref dac_output_chx
  *   @arg  DAC_OUTPUT_CH1 : DAC输出通道1-PA4
  *   @arg  DAC_OUTPUT_CH2 : DAC输出通道2-PA5
  * @param  value          : DAC输出值
  * @retval None
  */
void dac_output(uint8_t dacChx, uint16_t value)
{
    if(DAC_OUTPUT_CH1 == dacChx)
    {
        DAC_SetChannel1Data(DAC_Align_12b_R ,value);
        DAC_SoftwareTriggerCmd(DAC_Channel_1, ENABLE);
    }
    else if(DAC_OUTPUT_CH2 == dacChx)
    {
        DAC_SetChannel2Data(DAC_Align_12b_R ,value);
        DAC_SoftwareTriggerCmd(DAC_Channel_2, ENABLE);
    }
}
