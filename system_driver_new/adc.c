/* 包含头文件-----------------------------------------------------------------*/
#include "adc.h"

/* 私有宏定义-----------------------------------------------------------------*/
/* ADC1/2输入通道的引脚定义
ADC_IN0 PA0
ADC_IN1 PA1
ADC_IN2 PA2
ADC_IN3 PA3
ADC_IN4 PA4
ADC_IN5 PA5
ADC_IN6 PA6
ADC_IN7 PA7
ADC_IN8 PB0
ADC_IN9 PB1
*/

/* 私有类型定义---------------------------------------------------------------*/

/* 私有变量-------------------------------------------------------------------*/

/* 全局变量-------------------------------------------------------------------*/

/* 私有函数原型---------------------------------------------------------------*/


/**
  * @brief  ADC1初始化
  * @note   io初始化部分需用户自行修改
  * @param  None
  * @retval None
  */
void adc1_config(void)
{
    ADC_InitTypeDef ADC_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;
    
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE); /* 打开ADC时钟 */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB, ENABLE); /* 打开GPIO时钟 */

    /* ADC输入引脚的GPIO配置 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1; /* 设置引脚号 */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN; /* 设置引脚模式 模拟输入 */
    GPIO_Init(GPIOA, &GPIO_InitStructure); /* 初始化GPIO */
    
//    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0; /* 设置引脚号 */
//    GPIO_Init(GPIOB, &GPIO_InitStructure); /* 初始化GPIO */
    
    ADC_DeInit(ADC1);
    
    ADC_InitStructure.ADC_Mode = ADC_Mode_Independent; /* ADC模式 只用到ADC1时为单模式 */
    ADC_InitStructure.ADC_ScanConvMode = DISABLE; /* 扫描模式 是否从头到尾扫描ADC规则组内所有通道 */
    ADC_InitStructure.ADC_ContinuousConvMode = DISABLE; /* 连续转换模式 转换完组内最后一个通道时是否重头再来 */
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None; /* 外部触发模式 软件触发 */
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right; /* 转换结果对齐方式 右对齐 */
    ADC_InitStructure.ADC_NbrOfChannel = 1; /* 规则组内需转换的通道数目 */
    ADC_Init(ADC1, &ADC_InitStructure); /* 初始化ADC */
    
    ADC_Cmd(ADC1, ENABLE); /* 开启ADC */
    
    ADC_ResetCalibration(ADC1); // 初始化ADC校准寄存器  
    while(ADC_GetResetCalibrationStatus(ADC1)); // 等待校准寄存器初始化完成
    
    ADC_StartCalibration(ADC1); // ADC开始校准
    while(ADC_GetCalibrationStatus(ADC1)); // 等待校准完成
}

/**
  * @brief  ADC2初始化
  * @note   io初始化部分需用户自行修改
  * @param  None
  * @retval None
  */
void adc2_config(void)
{
    ADC_InitTypeDef ADC_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;
    
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC2, ENABLE); /* 打开ADC时钟 */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB, ENABLE); /* 打开GPIO时钟 */

    /* ADC输入引脚的GPIO配置 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1; /* 设置引脚号 */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN; /* 设置引脚模式 */
    GPIO_Init(GPIOA, &GPIO_InitStructure); /* 初始化GPIO */
    
//    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0; /* 设置引脚号 */
//    GPIO_Init(GPIOB, &GPIO_InitStructure); /* 初始化GPIO */
    
    ADC_DeInit(ADC2);
    
    ADC_InitStructure.ADC_Mode = ADC_Mode_Independent; /* ADC模式 只用到ADC1时为单模式 */
    ADC_InitStructure.ADC_ScanConvMode = DISABLE; /* 扫描模式 是否从头到尾扫描ADC规则组内所有通道 */
    ADC_InitStructure.ADC_ContinuousConvMode = DISABLE; /* 连续转换模式 转换完组内最后一个通道时是否重头再来 */
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None; /* 外部触发模式 软件触发 */
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right; /* 转换结果对齐方式 右对齐 */
    ADC_InitStructure.ADC_NbrOfChannel = 1; /* 规则组内需转换的通道数目 */
    ADC_Init(ADC2, &ADC_InitStructure); /* 初始化ADC */
    
    ADC_Cmd(ADC2, ENABLE); /* 开启ADC */
    
    ADC_ResetCalibration(ADC2); // 初始化ADC校准寄存器  
    while(ADC_GetResetCalibrationStatus(ADC2)); // 等待校准寄存器初始化完成
    
    ADC_StartCalibration(ADC2); // ADC开始校准
    while(ADC_GetCalibrationStatus(ADC2)); // 等待校准完成
}

/**
  * @brief  读取ADC1输入通道转换结果
  * @param  chx     : adc输入通道 @ref adc_input_chx
  *   @arg  ADC_IN0 : 输入通道0 PA0
  *   @arg  ADC_IN1 : 输入通道1 PA1
  *   @arg  ADC_IN2 : 输入通道2 PA2
  *   @arg  ADC_IN3 : 输入通道3 PA3
  *   @arg  ADC_IN4 : 输入通道4 PA4
  *   @arg  ADC_IN5 : 输入通道5 PA5
  *   @arg  ADC_IN6 : 输入通道6 PA6
  *   @arg  ADC_IN7 : 输入通道7 PA7
  *   @arg  ADC_IN8 : 输入通道8 PB0
  *   @arg  ADC_IN9 : 输入通道9 PB1
  * @retval 返回转换结果
  */
uint16_t adc1_get_conv_val(uint8_t chx)
{
    uint16_t val;
    
    /* 设置规则组通道 转换顺序和采样时间 */
    ADC_RegularChannelConfig(ADC1, chx, 1, ADC_SampleTime_55Cycles5);
    
    ADC_SoftwareStartConvCmd(ADC1, ENABLE); /* 使能软件启动转换 */
    while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC)); /* 等待转换完成 */
    val = ADC_GetConversionValue(ADC1); /* 获取转换结果 */
    
    return val;
}

/**
  * @brief  读取ADC2输入通道转换结果
  * @param  chx     : adc输入通道 @ref adc_input_chx
  *   @arg  ADC_IN0 : 输入通道0 PA0
  *   @arg  ADC_IN1 : 输入通道1 PA1
  *   @arg  ADC_IN2 : 输入通道2 PA2
  *   @arg  ADC_IN3 : 输入通道3 PA3
  *   @arg  ADC_IN4 : 输入通道4 PA4
  *   @arg  ADC_IN5 : 输入通道5 PA5
  *   @arg  ADC_IN6 : 输入通道6 PA6
  *   @arg  ADC_IN7 : 输入通道7 PA7
  *   @arg  ADC_IN8 : 输入通道8 PB0
  *   @arg  ADC_IN9 : 输入通道9 PB1
  * @retval 返回转换结果
  */
uint16_t adc2_get_conv_val(uint8_t chx)
{
    uint16_t val;
    
    /* 设置规则组通道 转换顺序和采样时间 */
    ADC_RegularChannelConfig(ADC2, chx, 1, ADC_SampleTime_55Cycles5);
    
    ADC_SoftwareStartConvCmd(ADC2, ENABLE); /* 使能软件启动转换 */
    while(!ADC_GetFlagStatus(ADC2, ADC_FLAG_EOC)); /* 等待转换完成 */
    val = ADC_GetConversionValue(ADC2); /* 获取转换结果 */
    
    return val;
}

/**
  * @brief  读取ADC1输入通道转换结果的平均值
  * @param  chx     : adc输入通道 @ref adc_input_chx
  *   @arg  ADC_IN0 : 输入通道0 PA0
  *   @arg  ADC_IN1 : 输入通道1 PA1
  *   @arg  ADC_IN2 : 输入通道2 PA2
  *   @arg  ADC_IN3 : 输入通道3 PA3
  *   @arg  ADC_IN4 : 输入通道4 PA4
  *   @arg  ADC_IN5 : 输入通道5 PA5
  *   @arg  ADC_IN6 : 输入通道6 PA6
  *   @arg  ADC_IN7 : 输入通道7 PA7
  *   @arg  ADC_IN8 : 输入通道8 PB0
  *   @arg  ADC_IN9 : 输入通道9 PB1
  * @param  num     : 多少次转换结果求平均
  * @retval 返回转换结果的平均值
  */
uint16_t adc1_get_average_val(uint8_t chx, uint16_t num)
{
    uint16_t tmp = num;
    uint32_t val = 0;
    
    /* 设置规则组通道 转换顺序和采样时间 */
    ADC_RegularChannelConfig(ADC1, chx, 1, ADC_SampleTime_55Cycles5);
    
    while(tmp--)
    {
        ADC_SoftwareStartConvCmd(ADC1, ENABLE); /* 使能软件启动转换 */
        while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC)); /* 等待转换完成 */
        val += ADC_GetConversionValue(ADC1); /* 获取转换结果 */
    }
    val = val / num;
    
    return val;
}

/**
  * @brief  读取ADC2输入通道转换结果的平均值
  * @param  chx     : adc输入通道 @ref adc_input_chx
  *   @arg  ADC_IN0 : 输入通道0 PA0
  *   @arg  ADC_IN1 : 输入通道1 PA1
  *   @arg  ADC_IN2 : 输入通道2 PA2
  *   @arg  ADC_IN3 : 输入通道3 PA3
  *   @arg  ADC_IN4 : 输入通道4 PA4
  *   @arg  ADC_IN5 : 输入通道5 PA5
  *   @arg  ADC_IN6 : 输入通道6 PA6
  *   @arg  ADC_IN7 : 输入通道7 PA7
  *   @arg  ADC_IN8 : 输入通道8 PB0
  *   @arg  ADC_IN9 : 输入通道9 PB1
  * @param  num     : 多少次转换结果求平均
  * @retval 返回转换结果的平均值
  */
uint16_t adc2_get_average_val(uint8_t chx, uint16_t num)
{
    uint16_t tmp = num;
    uint32_t val = 0;
    
    /* 设置规则组通道 转换顺序和采样时间 */
    ADC_RegularChannelConfig(ADC2, chx, 1, ADC_SampleTime_55Cycles5);
    
    while(tmp--)
    {
        ADC_SoftwareStartConvCmd(ADC2, ENABLE); /* 使能软件启动转换 */
        while(!ADC_GetFlagStatus(ADC2, ADC_FLAG_EOC)); /* 等待转换完成 */
        val += ADC_GetConversionValue(ADC2); /* 获取转换结果 */
    }
    val = val / num;
    
    return val;
}
