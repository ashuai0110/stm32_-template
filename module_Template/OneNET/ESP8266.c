#include "main.h"
//#include "stm32f10x.h"
//#include "ESP8266.h"
//#include "delay.h"
//#include "usart.h"
//#include <string.h>
//#include <stdio.h>

#define ESP8266_GPIO_PORT    	GPIOA			              /* GPIO端口 */
#define ESP8266_GPIO_CLK 	    RCC_APB2Periph_GPIOA		/* GPIO端口时钟 */
#define ESP8266_GPIO_PIN			GPIO_Pin_1			        /* 连接到SCL时钟线的GPIO */
#define ESP8266_UART					USART2									/* 串口号 */
// WIFI信息配置
#define ESP8266_WIFI_INFO			"AT+CWJAP=\"MNIAS\",\"0123456789\"\r\n"
// TCP信息配置 默认不更改
#define ESP8266_ONENET_INFO		"AT+CIPSTART=\"TCP\",\"183.230.40.39\",6002\r\n"

unsigned char ESP8266_buf[128];
unsigned short ESP8266_cnt = 0, ESP8266_cntPre = 0;


/**
  * @brief  OneNET使用串口的串口中断函数
  * @param  None
  * @retval None
  */
void USART2_IRQHandler(void)
{
	if(USART_GetITStatus(ESP8266_UART, USART_IT_RXNE) != RESET)
	{
		
		if(ESP8266_cnt >= sizeof(ESP8266_buf))	ESP8266_cnt = 0; //防止串口被刷爆
		ESP8266_buf[ESP8266_cnt++] = ESP8266_UART->DR;
		
		USART_ClearFlag(ESP8266_UART, USART_FLAG_RXNE);
	}
}
/**
  * @brief  ESP8266初始化
  * @param  None
  * @retval None
  */
void ESP8266_Init(void)
{
	GPIO_InitTypeDef GPIO_Initure;
	RCC_APB2PeriphClockCmd(ESP8266_GPIO_CLK, ENABLE);
	// ESP8266复位引脚
	GPIO_Initure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Initure.GPIO_Pin = ESP8266_GPIO_PIN;					
	GPIO_Initure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(ESP8266_GPIO_PORT, &GPIO_Initure);
	
	GPIO_WriteBit(ESP8266_GPIO_PORT, ESP8266_GPIO_PIN, Bit_RESET);
	DELAYClass.DelayMs(250);
	GPIO_WriteBit(ESP8266_GPIO_PORT, ESP8266_GPIO_PIN, Bit_SET);
	DELAYClass.DelayMs(500);
	
	ESP8266_Clear();
#ifdef DEBUG_printf
	printf("1. AT\r\n");
#endif
	while(ESP8266_SendCmd("AT\r\n", "OK")) DELAYClass.DelayMs(500);
#ifdef DEBUG_printf
	printf("2. CWMODE\r\n");
#endif
	while(ESP8266_SendCmd("AT+CWMODE=1\r\n", "OK")) DELAYClass.DelayMs(500);
//	printf( "3.AT+CWSTARTSMART=3\r\n");//支持ESP-Touch和Airkiss智能配网
//	while(ESP8266_SendCmd("AT+CWSTARTSMART=3\r\n", "GOT IP")) DELAYClass.DelayMs(500);
#ifdef DEBUG_printf
	printf("4. AT+CWDHCP\r\n");
#endif
	while(ESP8266_SendCmd("AT+CWDHCP=1,1\r\n", "OK")) DELAYClass.DelayMs(500);
#ifdef DEBUG_printf
	printf("5. CWJAP\r\n");
#endif
	while(ESP8266_SendCmd(ESP8266_WIFI_INFO, "GOT IP")) DELAYClass.DelayMs(500);
#ifdef DEBUG_printf	
	printf("6. CIPSTART\r\n");
#endif
	while(ESP8266_SendCmd(ESP8266_ONENET_INFO, "CONNECT")) DELAYClass.DelayMs(500);
#ifdef DEBUG_printf
	printf("7. ESP8266 Init OK\r\n");
#endif
}
/**
  * @brief  清空WIFI串口缓存区
  * @param  None
  * @retval None
  */
void ESP8266_Clear(void)
{
	memset(ESP8266_buf, 0, sizeof(ESP8266_buf));
	ESP8266_cnt = 0;
}
/**
  * @brief  等待接收完成
  * @param  None
  * @retval REV_OK-接收完成		REV_WAIT-接收超时未完成
	* @note   循坏调用检测是否接收完成
  */
_Bool ESP8266_WaitRecive(void)
{

	if(ESP8266_cnt == 0) 							//如果接收计数为0 则说明没有处于接收数据中，所以直接跳出，结束函数
		return REV_WAIT;

	if(ESP8266_cnt == ESP8266_cntPre)				//如果上一次的值和这次相同，则说明接收完毕
	{
		ESP8266_cnt = 0;							//清0接收计数
			
		return REV_OK;								//返回接收完成标志
	}

	ESP8266_cntPre = ESP8266_cnt;					//置为相同
	
	return REV_WAIT;								//返回接收未完成标志

}
/**
  * @brief  向WIFI串口发送命令
  * @param  cmd：命令字符串
	* @param  res：期待返回的内容
  * @retval 0-成功	1-失败
	* @note   None
  */
_Bool ESP8266_SendCmd(char *cmd, char *res)
{
	unsigned char timeOut = 200;

	USARTClass.USART_SendString1(ESP8266_UART, (unsigned char *)cmd, strlen((const char *)cmd));

	while(timeOut--)
	{
		// 如果收到数据
		if(ESP8266_WaitRecive() == REV_OK)							
		{
			// 如果检索到关键词
			if(strstr((const char *)ESP8266_buf, res) != NULL)
			{
				ESP8266_Clear(); // 清空缓存
				return 0;
			}
		}
		DELAYClass.DelayMs(10);
	}
	return 1;
}
/**
  * @brief  向WIFI串口TCP发送数据
  * @param  data：数据字符串
	* @param  len：长度
  * @retval None
	* @note   None
  */
void ESP8266_SendData(unsigned char *data, unsigned short len)
{
	char cmdBuf[32];
	
	ESP8266_Clear();								//清空接收缓存
	sprintf(cmdBuf, "AT+CIPSEND=%d\r\n", len);		//发送命令
	if(!ESP8266_SendCmd(cmdBuf, ">"))				//收到‘>’时可以发送数据
	{
		USARTClass.USART_SendString1(ESP8266_UART, data, len);		//发送设备连接请求数据
	}
}
/**
  * @brief  获取平台返回的数据
  * @param  timeOut：等待的时间(乘以10ms)
  * @retval 平台返回的原始数据
	* @note   不同网络设备返回的格式不同，需要去调试如ESP8266的返回格式为	"+IPD,x:yyy"	x代表数据长度，yyy是数据内容
  */
unsigned char *ESP8266_GetIPD(unsigned short timeOut)
{
	char *ptrIPD = NULL;
	
	do
	{
		if(ESP8266_WaitRecive() == REV_OK)								//如果接收完成
		{
			ptrIPD = strstr((char *)ESP8266_buf, "IPD,");				//搜索“IPD”头
			if(ptrIPD == NULL)											//如果没找到，可能是IPD头的延迟，还是需要等待一会，但不会超过设定的时间
			{
				//printf( "\"IPD\" not found\r\n");
			}
			else
			{
				ptrIPD = strchr(ptrIPD, ':');							//找到':'
				if(ptrIPD != NULL)
				{
					ptrIPD++;
					return (unsigned char *)(ptrIPD);
				}
				else
				{
					return NULL;
				}
			}
		}
		
		DELAYClass.DelayMs(5);													//延时等待
	} while(timeOut--);
	
	return NULL;														//超时还未找到，返回空指针
}
