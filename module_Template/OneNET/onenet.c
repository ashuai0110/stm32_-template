#include "main.h"
//#include "stm32f10x.h"
//#include "esp8266.h"
//#include "onenet.h"
//#include "mqttkit.h"
//#include "usart.h"
//#include "delay.h"
//#include <string.h>
//#include <stdio.h>


#define PROID			"509223"			// 产品ID
#define AUTH_INFO	"0123456789"	// 鉴权信息
#define DEVID			"932442653"		// 设备ID

/**
  * @brief  与onenet创建连接
  * @param  None
  * @retval 1-成功	0-失败
	* @note   与onenet平台建立连接
  */
_Bool OneNet_DevLink(void)
{
	MQTT_PACKET_STRUCTURE mqttPacket = {NULL, 0, 0, 0}; // 协议包
	unsigned char *dataPtr;
	_Bool status = 1;
#ifdef DEBUG_printf
	printf("OneNet_DevLink\n""PROID: %s,	AUIF: %s,	DEVID:%s\n", PROID, AUTH_INFO, DEVID);
#endif
	if(MQTT_PacketConnect(PROID, AUTH_INFO, DEVID, 256, 0, MQTT_QOS_LEVEL0, NULL, NULL, 0, &mqttPacket) == 0)
	{
		ESP8266_SendData(mqttPacket._data, mqttPacket._len);	// 上传平台
		dataPtr = ESP8266_GetIPD(250); // 等待平台响应
		if(dataPtr != NULL)
		{
			if(MQTT_UnPacketRecv(dataPtr) == MQTT_PKT_CONNACK)
			{
				switch(MQTT_UnPacketConnectAck(dataPtr))
				{
					case 0:
						status = 0;
#ifdef DEBUG_printf
						printf("Tips:	连接成功\n");
#endif
					break;
					case 1:
#ifdef DEBUG_printf
						printf("WARN:	连接失败：协议错误\n");
#endif
					break;
					case 2:
#ifdef DEBUG_printf
						printf("WARN:	连接失败：非法的clientid\n");
#endif
					break;
					case 3:
#ifdef DEBUG_printf
						printf("WARN:	连接失败：服务器失败\n");
#endif
					break;
					case 4:
#ifdef DEBUG_printf
						printf("WARN:	连接失败：用户名或密码错误\n");
#endif
					break;
					case 5:
#ifdef DEBUG_printf
						printf("WARN:	连接失败：非法链接(比如token非法)\n");
#endif
					break;
					default:
#ifdef DEBUG_printf
						printf("ERR:	连接失败：未知错误\n");
#endif
					break;
				}
			}
		}
		MQTT_DeleteBuffer(&mqttPacket); // 删包
	}
	else
	{
#ifdef DEBUG_printf
		printf("WARN:	MQTT_PacketConnect Failed\n");
#endif
	}
	return status;
}
/**
  * @brief  填充上传数据
  * @param  buf：数据
  * @retval 数据长度
	* @note   键值对格式逗号分隔 例如 {key1:99,key2:0}
  */
unsigned char OneNet_FillBuf(char *buf)
{
	char text[128];
	memset(text, 0, sizeof(text));
	
	strcpy(buf, "{");
	/****************user handle********************/
	memset(text, 0, sizeof(text));
	sprintf(text, "\"temp\":%d,", AllFlag.waterTemp);
	strcat(buf, text);
   
	memset(text, 0, sizeof(text));
	sprintf(text, "\"RELAY1\":%d,",AllFlag.relay1_sta);
	strcat(buf, text);

	memset(text, 0, sizeof(text));
	sprintf(text, "\"RELAY2\":%d,",AllFlag.relay2_sta);
	strcat(buf, text);
	
	memset(text, 0, sizeof(text));
	sprintf(text, "\"tempMax\":%d,",AllFlag.waterTempMax);
	strcat(buf, text);
	
	memset(text, 0, sizeof(text));
	sprintf(text, "\"tempMin\":%d",AllFlag.waterTempMin);
	strcat(buf, text);
	/**********************************************/
	strcat(buf, "}");
	
	return strlen(buf);
}
/**
  * @brief  上传数据到OneNET平台
  * @param  None
  * @retval None
  */
void OneNet_SendData(void)
{
	MQTT_PACKET_STRUCTURE mqttPacket = {NULL, 0, 0, 0};	// 协议包
	char buf[128];
	short body_len = 0, i = 0;
#ifdef DEBUG_printf
	printf("Tips:	OneNet_SendData-MQTT\r\n");
#endif
	memset(buf, 0, sizeof(buf));
	body_len = OneNet_FillBuf(buf); // 获取当前需要发送的数据流的总长度
	if(body_len)
	{
		if(MQTT_PacketSaveData(DEVID, body_len, NULL, 3, &mqttPacket) == 0) // 封包
		{
			for(; i < body_len; i++) mqttPacket._data[mqttPacket._len++] = buf[i];
			ESP8266_SendData(mqttPacket._data, mqttPacket._len); // 上传数据到平台
#ifdef DEBUG_printf
			printf("Send %d Bytes\r\n", mqttPacket._len);
#endif
			MQTT_DeleteBuffer(&mqttPacket); // 删包
		}
		else
		{
#ifdef DEBUG_printf
			printf("WARN:	EDP_NewBuffer Failed\r\n");
#endif
		}
	}
}
/**
  * @brief  平台返回数据检测
  * @param  cmd：平台返回的数据
  * @retval None
  */
void OneNet_RevPro(unsigned char *cmd)
{
	MQTT_PACKET_STRUCTURE mqttPacket = {NULL, 0, 0, 0}; // 协议包
	char *req_payload = NULL;
	char *cmdid_topic = NULL;
	unsigned short req_len = 0;
	unsigned char type = 0;
	short result = 0;
	char *dataPtr = NULL;
	char numBuf[10];
	int num = 0;
	type = MQTT_UnPacketRecv(cmd);
	switch(type)
	{
		case MQTT_PKT_CMD: // 命令下发
			result = MQTT_UnPacketCmd(cmd, &cmdid_topic, &req_payload, &req_len);	// 解出topic和消息体
			if(result == 0)
			{
#ifdef DEBUG_printf
				printf("cmdid: %s, req: %s, req_len: %d\r\n", cmdid_topic, req_payload, req_len);
#endif
				if(MQTT_PacketCmdResp(cmdid_topic, req_payload, &mqttPacket) == 0) // 命令回复组包
				{
#ifdef DEBUG_printf
					printf("Tips:	Send CmdResp\r\n");
#endif
					ESP8266_SendData(mqttPacket._data, mqttPacket._len); // 回复命令
					MQTT_DeleteBuffer(&mqttPacket); // 删包
				}
			}
			break;
		case MQTT_PKT_PUBACK: //发送Publish消息，平台回复的Ack
			if(MQTT_UnPacketPublishAck(cmd) == 0)
			{
#ifdef DEBUG_printf
				printf("Tips:	MQTT Publish Send OK\r\n");
#endif
			}
			break;
		default:
			result = -1;
		break;
	}
	ESP8266_Clear(); // 清空缓存
	if(result == -1) return ;
	/************************user handle*****************************/
	if(strstr((char *)req_payload, "OPEN1")) // 搜索"OPEN1"
	{
#ifdef DEBUG_printf
		printf("OPEN1\n");
#endif
		AllFlag.AutoCtrl_time = 0;
		AllFlag.AutoCtrl_flag = 1;
		GPIOClass.Set(RELAY1);
		// user handle
	}
	if(strstr((char *)req_payload, "CLOSE1")) // 搜索"CLOSE1"
	{
		AllFlag.AutoCtrl_time = 0;
		AllFlag.AutoCtrl_flag = 1;
		GPIOClass.Reset(RELAY1);
		// user handle
	}
	if(strstr((char *)req_payload, "OPEN2")) // 搜索"OPEN2"
	{
		AllFlag.AutoCtrl_time = 0;
		AllFlag.AutoCtrl_flag = 1;
		GPIOClass.Set(RELAY2);
		// user handle
	}
	if(strstr((char *)req_payload, "CLOSE2")) // 搜索"CLOSE2"
	{
		AllFlag.AutoCtrl_time = 0;
		AllFlag.AutoCtrl_flag = 1;
		GPIOClass.Reset(RELAY2);
		// user handle
	}
	
	dataPtr = strchr(req_payload, ':'); // 搜索':'
    
	if(dataPtr != NULL && result != -1) // 如果找到了
	{
		dataPtr++;
		while(*dataPtr >= '0' && *dataPtr <= '9')	// 判断是否是下发的命令控制数据
		{
			numBuf[num++] = *dataPtr++;
		}
		numBuf[num] = 0;
		
		num = atoi((const char *)numBuf); // 转为数值形式
    /*******************************************************/
    if(strstr((char *)req_payload, "xxx")) // 搜索"xxx"
		{
			AllFlag.xxx=num;
#ifdef DEBUG_printf
			printf("xxx:%d\n",num);
#endif
		}
		AllFlag.OneNET_send_flag = 1; // 发送标志置位，立即发送
      
  }
	if(type == MQTT_PKT_CMD || type == MQTT_PKT_PUBLISH)
	{
		MQTT_FreeBuffer(cmdid_topic);
		MQTT_FreeBuffer(req_payload);
	}
}
