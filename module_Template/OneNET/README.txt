ESP8266连接OneNET云平台

需要配合system_Template中部分组件使用，如USART，DELAY等

/**********************Init 举例************************/
USARTClass.USART2_Init(115200); // ESP8266串口初始化
ESP8266_Init(); // 联网配置初始化
while(OneNet_DevLink()) DELAYClass.DelayMs(500); // 接入OneNET

/***********************发送举例************************/
// 5s周期向OneNET发送
if(AllFlag.OneNET_send_flag)
{
	OneNet_SendData(); // 发送数据
	ESP8266_Clear();
	AllFlag.OneNET_send_flag = 0;
}

/***********************读取举例************************/
// 10ms周期从OneNET读取
if(AllFlag.OneNET_rec_flag)
{
	dataPtr = ESP8266_GetIPD(0);
	if(dataPtr != NULL)	OneNet_RevPro(dataPtr);
	AllFlag.OneNET_rec_flag = 0;
}