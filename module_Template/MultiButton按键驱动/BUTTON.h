#ifndef __BUTTON_AS_H
#define __BUTTON_AS_H	 

#include "main.h" 	   

typedef struct {
	void (* Init)(void);
	void (* Scan)(void);
	uint8_t (* Read_key1)(void);
	uint8_t (* Read_key2)(void);
} KEYClassStruct;

extern KEYClassStruct KEYClass;

#endif
