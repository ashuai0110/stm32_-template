#include "main.h"
#include "MultiButton.h"

static void KEY_GPIO_Config(void);
static uint8_t read_key1_pin(void);
static uint8_t read_key2_pin(void);
static void key1_shortPress_handle(void *btn);
static void key2_shortPress_handle(void *btn);
static void key1_longPress_handle(void *btn);
static void key2_longPress_handle(void *btn);
	
static void KEY_Init(void);
static void KEY_Scan(void);

KEYClassStruct KEYClass = {
	.Init = KEY_Init,
	.Scan = KEY_Scan,
	.Read_key1 = read_key1_pin,
	.Read_key2 = read_key2_pin
};

// 按键结构体
struct Button key1;
struct Button key2;

#define KEY1_PORT_CLK		(RCC_APB2Periph_GPIOB)
#define KEY1_PORT				GPIOB
#define KEY1_PIN				GPIO_Pin_6
#define KEY2_PORT_CLK		(RCC_APB2Periph_GPIOB)
#define KEY2_PORT				GPIOB
#define KEY2_PIN				GPIO_Pin_7

static void KEY_GPIO_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
  /*开启按键GPIO口的时钟*/
  RCC_APB2PeriphClockCmd(KEY1_PORT_CLK | KEY2_PORT_CLK,ENABLE);
  /*初始化GPIO*/
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
  GPIO_InitStructure.GPIO_Pin = KEY1_PIN;
  GPIO_Init(KEY1_PORT,&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = KEY2_PIN;
  GPIO_Init(KEY2_PORT,&GPIO_InitStructure);
}

static uint8_t read_key1_pin(void)
{
	return GPIO_ReadInputDataBit(KEY1_PORT, KEY1_PIN);
}
static uint8_t read_key2_pin(void)
{
	return GPIO_ReadInputDataBit(KEY2_PORT, KEY2_PIN);
}
// key1短按切换页面
static void key1_shortPress_handle(void *btn)
{

	#ifdef DEBUG_printf
		printf("key1 short press\n");
	#endif
}
// key2短按切换选项																																																																																																																																																																																																																																									·················
static void key2_shortPress_handle(void *btn)
{
	#ifdef DEBUG_printf
		printf("key2 short press\n");
	#endif
}
// key1长按
static void key1_longPress_handle(void *btn)
{

	#ifdef DEBUG_printf
		printf("key1 long press\n");
	#endif
}
// key2长按
static void key2_longPress_handle(void *btn)
{

	#ifdef DEBUG_printf
		printf("key2 long press\n");
	#endif
}

static void KEY_Init(void)
{
	KEY_GPIO_Config();
	button_init(&key1, read_key1_pin, 0);
	button_init(&key2, read_key2_pin, 0);
	button_attach(&key1, SINGLE_CLICK, key1_shortPress_handle);
	button_attach(&key2, SINGLE_CLICK, key2_shortPress_handle);
	button_attach(&key1, LONG_PRESS_START, key1_longPress_handle);
	button_attach(&key2, LONG_PRESS_START, key2_longPress_handle);
	button_start(&key1);
	button_start(&key2);
}

static void KEY_Scan(void)
{
	button_ticks();
}

