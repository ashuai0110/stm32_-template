#ifndef __I2C_H
#define __I2C_H

#include "main.h"

void IIC_start(void);
void IIC_stop(void);
void IIC_send_byte(uint8_t _ucByte);
uint8_t IIC_read_byte(uint8_t ack);
uint8_t IIC_wait_ack(void);
void IIC_ack(void);
void IIC_nack(void);
void IIC_init(void);

#endif
