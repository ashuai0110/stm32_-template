#include <stdio.h>
#include "MPU6050.h"

int main()
{
	short gyro[3]; // 陀螺仪xyz
	short acce[3]; // 加速度xyz
	MPU6050_Init(); // 初始化
	while(1) {
		MPU6050ReadGyro(gyro); // 读取三轴陀螺仪
		MPU6050ReadAcc(acce); // 读取三轴加速度
		printf("gyro x:%d y:%d z:%d, acce x:%d y:%d z:%d\n", gyro[0], gyro[1], gyro[2], acce[0], acce[1], acce[2]);
		delayMs(1000);
	}
}