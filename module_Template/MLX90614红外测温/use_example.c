#include <stdio.h>
#include "MLX90614.h"

int main()
{
	float tem;
	SMBus_Init(); // 初始化
	while(1) {
		tem = SMBus_ReadTemp(); // 读取温度
		printf("tem:%f\n", tem);
		delayMs(1000);
	}
}