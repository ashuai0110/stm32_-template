#ifndef __HEALTH_AS_H
#define __HEALTH_AS_H

#include "main.h" 	   

typedef struct {
	uint8_t head; // 数据头
	int8_t heartRateWaveform[64]; // 心率波形数据
	uint8_t heartRate; // 心率
	uint8_t spo; // 血氧
	uint8_t bk; // 微循环
	uint8_t rsv[8]; // 保留数据 rsv[3]收缩压 rsv[4]舒张压
}HealthDataStruct;

typedef struct {
	void (* Init)(void);
	void (* GetHealthData)(HealthDataStruct* HealthData1);
	void (* Open)(void);
	void (* Close)(void);
}HealthClassStruct;

extern HealthClassStruct HealthClass;

void Health_TIM_Init(void);
void Health_USART_Init(void);

#endif
