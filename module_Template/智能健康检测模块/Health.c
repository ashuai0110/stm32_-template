/*
智能健康检测模块
*/
#include "main.h"	 

static void Health_Rec_Handle(HealthDataStruct *HealthData1);
static void Health_TIM_Init(void);
static void Health_USART_Init(void);
static void Health_Init(void);
static void Health_Open(void);
static void Health_Close(void);

HealthClassStruct HealthClass = {
	.Init = Health_Init,
	.GetHealthData = Health_Rec_Handle,
	.Open = Health_Open,
	.Close = Health_Close
};

#define Health_MAX_RECV_LEN		128		//	接收缓存大小
#define Health_MAX_SEND_LEN		1		//  发送缓存大小
#define Health_RX_EN 					1			//  接收使能标志

// 串口接收和发送缓存区
unsigned char Health_RX_BUF[Health_MAX_RECV_LEN]; 				
char Health_TX_BUF[Health_MAX_SEND_LEN]; 				
// [14:0]: 接收字节数 [15]: 接收完成标志
volatile uint16_t Health_RX_STA = 0; 

#if 1
#define  Healthx                   USART1
#define  Health_CLK                RCC_APB2Periph_USART1
#define  Health_APBxClkCmd         RCC_APB2PeriphClockCmd
#define  Health_BAUDRATE           38400

#define  Health_GPIO_CLK           (RCC_APB2Periph_GPIOA)
#define  Health_GPIO_APBxClkCmd    RCC_APB2PeriphClockCmd
    
#define  Health_TX_GPIO_PORT       GPIOA   
#define  Health_TX_GPIO_PIN        GPIO_Pin_9
#define  Health_RX_GPIO_PORT       GPIOA
#define  Health_RX_GPIO_PIN        GPIO_Pin_10

#define  Health_IRQ                USART1_IRQn
#define  Health_IRQHandler         USART1_IRQHandler
#else
#define  Healthx                   USART2
#define  Health_CLK                RCC_APB1Periph_USART2
#define  Health_APBxClkCmd         RCC_APB1PeriphClockCmd
#define  Health_BAUDRATE           115200
                                        
#define  Health_GPIO_CLK           (RCC_APB2Periph_GPIOA)
#define  Health_GPIO_APBxClkCmd    RCC_APB2PeriphClockCmd
                                        
#define  Health_TX_GPIO_PORT       GPIOA   
#define  Health_TX_GPIO_PIN        GPIO_Pin_2
#define  Health_RX_GPIO_PORT       GPIOA
#define  Health_RX_GPIO_PIN        GPIO_Pin_3
                                        
#define  Health_IRQ                USART2_IRQn
#define  Health_IRQHandler         USART2_IRQHandler
#endif  	

/**
  * @brief  串口中断
	* @param  None
  * @retval None
  * @note   None
*/
void Health_IRQHandler(void)
{
	uint8_t res;	      
	if(USART_GetITStatus(Healthx, USART_IT_RXNE) != RESET)
	{	 
		res =USART_ReceiveData(Healthx);		 
		if((Health_RX_STA&(1<<15))==0)
		{ 
			if(Health_RX_STA<Health_MAX_RECV_LEN)
			{
				TIM_SetCounter(TIM3,0);
				if(Health_RX_STA==0) 
				{
					TIM_Cmd(TIM3,ENABLE);
				}
				Health_RX_BUF[Health_RX_STA++]=res;	
			}else 
			{
				Health_RX_STA|=1<<15;
			} 
		}
	}  				 											 
}   
/**
  * @brief  TIM3中断
	* @param  None
  * @retval None
  * @note   None
*/
void TIM3_IRQHandler(void)
{ 	
	if(TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)
	{	 			   
		Health_RX_STA|=1<<15;
		TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
		TIM_Cmd(TIM3, DISABLE); 
	}	    
}
/**
  * @brief  健康检测模块数据处理
	* @param  None
  * @retval None
  * @note   把串口接收到的数据分类存放
*/
static void Health_Rec_Handle(HealthDataStruct *HealthData1)
{
	// 接收到一次数据了
	if(Health_RX_STA&0X8000)		
	{ 
		// 添加结束符
		Health_RX_BUF[Health_RX_STA&0X7FFF]=0;
#ifdef DEBUG_printf
		//printf("%s\n",Health_RX_BUF);
#endif
		// 数据头为0xff且第76字节不为空
		if((Health_RX_BUF[0] == 0xFF) && (Health_RX_BUF[75]) != 0)
		{
			HealthData1->head = Health_RX_BUF[0];
			uint8_t i = 0;
			for(i = 0; i <= 64; i++)
			{
				HealthData1->heartRateWaveform[i] = Health_RX_BUF[i+1];
			}
			HealthData1->heartRate = Health_RX_BUF[65];
			HealthData1->spo = Health_RX_BUF[66];
			HealthData1->bk = Health_RX_BUF[67];
			for(i = 0; i <= 8; i++)
			{
				HealthData1->rsv[i] = Health_RX_BUF[i+68];
			}
#ifdef DEBUG_printf
			printf("gaoya: %d diya: %d\n",HealthData1->rsv[3], HealthData1->rsv[4]);
#endif
		}
		// 清空缓存区
		memset(Health_RX_BUF, 0, Health_MAX_RECV_LEN);
		// 清除接收标志
		Health_RX_STA = 0;
	} 
}
/**
  * @brief  开启检测
	* @param  None
  * @retval None
  * @note   None
*/
static void Health_Open(void)
{
	USART_SendData(Healthx, 0x8A);
}
/**
  * @brief  关闭检测
	* @param  None
  * @retval None
  * @note   None
*/
static void Health_Close(void)
{
	USART_SendData(Healthx, 0x88);
}
/**
  * @brief  10ms定时器初始化
	* @param  None
  * @retval None
  * @note   用于检测串口接收状态
*/
static void Health_TIM_Init(void)
{	
	NVIC_InitTypeDef NVIC_InitStructure;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	
	TIM_DeInit(TIM3);
	TIM_TimeBaseStructure.TIM_Period = 1000-1; 
	TIM_TimeBaseStructure.TIM_Prescaler = 7200-1; 
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; 
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; 
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);
	TIM_Cmd(TIM3,DISABLE);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}
/**
  * @brief  串口初始化
	* @param  None
  * @retval None
  * @note   None
*/
static void Health_USART_Init(void)
{  
	NVIC_InitTypeDef NVIC_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;

	Health_GPIO_APBxClkCmd(Health_GPIO_CLK, ENABLE);
	Health_APBxClkCmd(Health_CLK,ENABLE);

 	USART_DeInit(Healthx); 
	//USART_TX
  GPIO_InitStructure.GPIO_Pin = Health_TX_GPIO_PIN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(Health_TX_GPIO_PORT, &GPIO_InitStructure);
  //USART_RX
  GPIO_InitStructure.GPIO_Pin = Health_RX_GPIO_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(Health_RX_GPIO_PORT, &GPIO_InitStructure);
	
	USART_InitStructure.USART_BaudRate = Health_BAUDRATE;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	
	USART_Init(Healthx, &USART_InitStructure);
	
	USART_Cmd(Healthx, ENABLE);
  USART_ITConfig(Healthx, USART_IT_RXNE, ENABLE);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	NVIC_InitStructure.NVIC_IRQChannel = Health_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	Health_RX_STA=0;
}
/**
  * @brief  健康检测模块初始化
	* @param  None
  * @retval None
  * @note   None
*/
static void Health_Init(void)
{
	Health_USART_Init();
	Health_TIM_Init();
}
