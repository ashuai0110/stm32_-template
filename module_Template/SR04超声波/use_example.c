#include <stdio.h>
#include "SR04.h"

int main()
{
	uint16_t distance = 0;
	SR04.Init(); // 初始化
	while(1) {
		distance = SR04.Read(); // 读取距离cm
		printf("sr04 distance:%d\n", distance);
		delayMs(100);
	}
}