#ifndef __SR04_H_
#define __SR04_H_

#include <main.h>

typedef struct
{
    void (*Init)(void);
    uint16_t (*Read)(void);
}SR04_t;

extern SR04_t SR04;

#endif
