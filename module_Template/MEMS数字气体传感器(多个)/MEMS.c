#include "MEMS.h"
#include "IICs.h"


/**
  * @brief  MEMS数字型气体传感器IIC端口初始化
  * @param  None
  * @retval	None
  */
void MEMS_init(uint8_t iicNo)
{
	IIC_init(iicNo); // iic延时计数400
}
/**
  * @brief  读取MEMS数字型气体传感器检测浓度
  * @param  None
  * @retval 返回读取到的数据 2字节
  */
uint16_t MEMS_read(uint8_t iicNo)
{
	uint8_t recHigh = 0,recLow = 0;
	IIC_start(iicNo);
	IIC_send_byte(iicNo, MEMS_wrAddr); // slave address + write cmd
	if(IIC_wait_ack(iicNo)) {
		IIC_stop(iicNo);
		return 0xFFFF;
	}
	IIC_send_byte(iicNo, 0xA1); // read cmd
	if(IIC_wait_ack(iicNo)) {
		IIC_stop(iicNo);
		return 0xFFFE;
	}
	IIC_start(iicNo);
	IIC_send_byte(iicNo, MEMS_rdAddr); // slave address + read cmd
	if(IIC_wait_ack(iicNo)) {
		IIC_stop(iicNo);
		return 0xFFFD;
	}
	recHigh = IIC_read_byte(iicNo, 1);
	recLow = IIC_read_byte(iicNo, 0);
	IIC_stop(iicNo);
	return (uint16_t)recHigh << 8 | recLow;
}
