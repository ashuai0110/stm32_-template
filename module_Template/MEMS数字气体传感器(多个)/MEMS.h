#ifndef __MEMS_H
#define __MEMS_H

#include "main.h"

#define MEMS_wrAddr	0x54
#define MEMS_rdAddr	0x55

void MEMS_init(uint8_t iicNo);
uint16_t MEMS_read(uint8_t iicNo);

#endif
