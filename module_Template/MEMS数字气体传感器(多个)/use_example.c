#include <stdio.h>
#include "MEMS.h"

int main()
{
	uint16_t NH3_val, H2S_val;
	MEMS_init(IIC_MEMS_NH3); // 初始化
	MEMS_init(IIC_MEMS_H2S); // 初始化
	while(1) {
		NH3_val = MEMS_read(IIC_MEMS_NH3); // 读取
		H2S_val = MEMS_read(IIC_MEMS_H2S); // 读取
		printf("NH3 ppm:%d, H2S ppm:%d\n", NH3_val, H2S_val);
		delayMs(1000);
	}
}