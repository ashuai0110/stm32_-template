#ifndef __IICS_H
#define __IICS_H

#include "main.h"

typedef enum {
	IIC_MEMS_NH3 = 0,
	IIC_MEMS_H2S
} IIC_NO;

void Set_IIC_SDA_OUT(uint8_t iicNo);
void Set_IIC_SDA_IN(uint8_t iicNo);
void IIC_init(uint8_t iicNo);
void IIC_start(uint8_t iicNo);
void IIC_stop(uint8_t iicNo);
void IIC_ack(uint8_t iicNo);
void IIC_noack(uint8_t iicNo);
uint8_t IIC_wait_ack(uint8_t iicNo);
void IIC_send_byte(uint8_t iicNo, uint8_t txd);
uint8_t IIC_read_byte(uint8_t iicNo, uint8_t ack);
#endif
