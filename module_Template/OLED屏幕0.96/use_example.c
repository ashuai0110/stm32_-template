#include <stdio.h>
#include "OLED.h"

int main()
{
	OLED_Init(); // 初始化
	OLED_Clear(); // 清屏
	while(1) {
		OLED_ShowNum(0, 0, 123456, 6, 16); // 第一行
		OLED_ShowChar(0, 2, '?', 16); // 第二行
		OLED_ShowString(0, 4, "abcd", 16); // 第三行
		OLED_ShowCHinese(0, 6, 0); // 第四行
		/* 汉字大小16*16 数字和字符大小8*16 128*64OLED最多四行
		123456
		?
		abcd
		智
		*/
	}
}