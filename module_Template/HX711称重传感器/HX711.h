#ifndef __HX711_H_
#define __HX711_H_


#include <stdint.h>

typedef struct
{
    void (*Init)(void);
    uint32_t (*GetValue)(void);
}HX711_t;


/* extern variables ------------------------------------------------*/
extern HX711_t HX711;

/* extern function prototypes --------------------------------------*/



#endif

