#include <stdio.h>
#include "HX711.h"

int main()
{
	int weight = 0;
	HX711.Init(); // 初始化
	while(1) {
		weight = HX711.GetValue();
		printf("hx711 weight:%d\n", weight);
		delayMs(1000);
	}
}