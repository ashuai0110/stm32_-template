/* include ---------------------------------------------------------*/
#include <main.h>



/* private define -------------------------------------------------*/
// SCK=Tx DT=Rx
#define HX711_SCK_GPIO_PORT    			GPIOA			              /* GPIO端口 */
#define HX711_SCK_GPIO_CLK 	    		RCC_APB2Periph_GPIOA		/* GPIO端口时钟 */
#define HX711_SCK_GPIO_PIN				GPIO_Pin_4			        /* 连接到SCL时钟线的GPIO */

#define HX711_DT_GPIO_PORT    			GPIOA			              /* GPIO端口 */
#define HX711_DT_GPIO_CLK 	    		RCC_APB2Periph_GPIOA		/* GPIO端口时钟 */
#define HX711_DT_GPIO_PIN				GPIO_Pin_5			        /* 连接到SCL时钟线的GPIO */

/* private variables ----------------------------------------------*/
 uint32_t HX711_Buffer = 0;

/* private function prototypes ------------------------------------*/
static void Init(void);
static uint32_t Read(void);
static uint32_t GetValue(void);

/* public variables -----------------------------------------------*/
HX711_t HX711 =
{
    Init,
    GetValue
};

/**
  * @brief  初始化
  * @param  None
  * @retval None
  * @note   None
*/
static void Init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_APB2PeriphClockCmd(HX711_SCK_GPIO_CLK | HX711_DT_GPIO_CLK, ENABLE);
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Pin = HX711_DT_GPIO_PIN;
	GPIO_Init(HX711_DT_GPIO_PORT, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Pin = HX711_SCK_GPIO_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(HX711_SCK_GPIO_PORT, &GPIO_InitStructure);
	
	DELAYClass.DelayMs(500);
	
	HX711_Buffer = Read();
	
	DELAYClass.DelayMs(500);
	
	HX711_Buffer = Read();
}

/**
  * @brief  读取数据
  * @param  None
  * @retval None
  * @note   None
*/
static uint32_t Read(void)
{
	uint8_t i;
	uint32_t value = 0;
	uint16_t cnt = 65535;
	
	/**
	数据手册写到，当数据输出管脚 DOUT 为高电平时，表明A/D 转换器还未准备好输出数据，此时串口时
	钟输入信号 PD_SCK 应为低电平，所以下面设置引脚状态。
	**/
	GPIO_SetBits(HX711_DT_GPIO_PORT, HX711_DT_GPIO_PIN); //初始状态DT引脚为高电平
	GPIO_ResetBits(HX711_SCK_GPIO_PORT, HX711_SCK_GPIO_PIN); //初始状态SCK引脚为低电平
	
	/**
	等待DT引脚变为高电平
	**/
	while(GPIO_ReadInputDataBit(HX711_DT_GPIO_PORT, HX711_DT_GPIO_PIN)) {
		cnt--;
		if(!cnt) break;
	}
	DELAYClass.DelayUs(1);
	
	/**
	当 DOUT 从高电平变低电平后，PD_SCK 应输入 25 至 27 个不等的时钟脉冲
	25个时钟脉冲 ---> 通道A 增益128
	26个时钟脉冲 ---> 通道B 增益32
	27个时钟脉冲 ---> 通道A 增益64
	**/
	for(i=0; i<24; i++) //24位输出数据从最高位至最低位逐位输出完成
	{
		//方法二：
		GPIO_SetBits(HX711_SCK_GPIO_PORT, HX711_SCK_GPIO_PIN);
		DELAYClass.DelayUs(1);
		GPIO_ResetBits(HX711_SCK_GPIO_PORT, HX711_SCK_GPIO_PIN);
		if(GPIO_ReadInputDataBit(HX711_DT_GPIO_PORT, HX711_DT_GPIO_PIN) == 0)
		{
			value = value << 1;
			value |= 0x00;
		}
		if(GPIO_ReadInputDataBit(HX711_DT_GPIO_PORT, HX711_DT_GPIO_PIN) == 1)
		{
			value = value << 1;
			value |= 0x01;
		}
		DELAYClass.DelayUs(1);
	}
	
	//第 25至 27 个时钟脉冲用来选择下一次 A/D 转换的输入通道和增益
	GPIO_SetBits(HX711_SCK_GPIO_PORT, HX711_SCK_GPIO_PIN); 
	value = value^0x800000; 
	DELAYClass.DelayUs(1); 
	GPIO_ResetBits(HX711_SCK_GPIO_PORT, HX711_SCK_GPIO_PIN); 
	DELAYClass.DelayUs(1);  
	return value;
}


/**
  * @brief  读取数据
  * @param  None
  * @retval None
  * @note   None
*/
static uint32_t GetValue(void)
{
	uint32_t value_t = Read();
	uint32_t Weight_Shiwu = 0;

	if ( value_t > HX711_Buffer )
	{
		Weight_Shiwu	= value_t;
		Weight_Shiwu	= Weight_Shiwu - HX711_Buffer;                          /* 获取实物的AD采样数值。 */

		Weight_Shiwu = (unsigned long) ( (float) Weight_Shiwu / 0.38755);      /* 计算实物的实际重量 */


		/*
		 * 因为不同的传感器特性曲线不一样，因此，每一个传感器需要矫正这里的GapValue这个除数。
		 * 当发现测试出来的重量偏大时，增加该数值。
		 * 如果测试出来的重量偏小时，减小改数值。
		 * 该数值一般在4.0-5.0之间。因传感器不同而定。
		 * +0.05是为了四舍五入百分位
		 */
	}
	
	return Weight_Shiwu/1000;
	
}

