#include <stdio.h>
#include "MEMS.h"

int main()
{
	uint16_t mems_val;
	MEMS_CH2O_init(); // 初始化
	while(1) {
		mems_val = MEMS_CH2O_read(); // 读取
		printf("mems read ppm:%d\n", mems_val);
		delayMs(1000);
	}
}