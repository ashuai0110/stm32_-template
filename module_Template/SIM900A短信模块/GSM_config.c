#include "main.h"


#if 0
#define  GSM_USARTx                   USART1
#define  GSM_USART_CLK                RCC_APB2Periph_USART1
#define  GSM_USART_APBxClkCmd         RCC_APB2PeriphClockCmd
#define  GSM_USART_BAUDRATE           9600

#define  GSM_USART_GPIO_CLK           (RCC_APB2Periph_GPIOA)
#define  GSM_USART_GPIO_APBxClkCmd    RCC_APB2PeriphClockCmd
    
#define  GSM_USART_TX_GPIO_PORT       GPIOA   
#define  GSM_USART_TX_GPIO_PIN        GPIO_Pin_9
#define  GSM_USART_RX_GPIO_PORT       GPIOA
#define  GSM_USART_RX_GPIO_PIN        GPIO_Pin_10

#define  GSM_USART_IRQ                USART1_IRQn
#define  GSM_USART_IRQHandler         USART1_IRQHandler

#else
#define  GSM_USARTx                   USART2
#define  GSM_USART_CLK                RCC_APB1Periph_USART2
#define  GSM_USART_APBxClkCmd         RCC_APB1PeriphClockCmd
#define  GSM_USART_BAUDRATE           9600
                                        
#define  GSM_USART_GPIO_CLK           (RCC_APB2Periph_GPIOA)
#define  GSM_USART_GPIO_APBxClkCmd    RCC_APB2PeriphClockCmd
                                        
#define  GSM_USART_TX_GPIO_PORT       GPIOA   
#define  GSM_USART_TX_GPIO_PIN        GPIO_Pin_2
#define  GSM_USART_RX_GPIO_PORT       GPIOA
#define  GSM_USART_RX_GPIO_PIN        GPIO_Pin_3
                                        
#define  GSM_USART_IRQ                USART2_IRQn
#define  GSM_USART_IRQHandler         USART2_IRQHandler
#endif

//	
char GSM_USART_RX_BUF[GSM_USART_MAX_RECV_LEN]; 				
char GSM_USART_TX_BUF[GSM_USART_MAX_SEND_LEN]; 				

// [14:0]: 接收字节数 [15]: 接收完成标志
volatile uint16_t GSM_USART_RX_STA = 0;   	

// GSM串口中断处理
void GSM_USART_IRQHandler(void)
{
	uint8_t res;	      
	if(USART_GetITStatus(GSM_USARTx, USART_IT_RXNE) != RESET)
	{	 
		res = USART_ReceiveData(GSM_USARTx);
		if((GSM_USART_RX_STA&(1<<15))==0)
		{ 
			if(GSM_USART_RX_STA<GSM_USART_MAX_RECV_LEN)
			{
				TIM_SetCounter(TIM3,0);
				if(GSM_USART_RX_STA==0) 
				{
					TIM_Cmd(TIM3,ENABLE);
				}
				GSM_USART_RX_BUF[GSM_USART_RX_STA++]=res;	
			}else 
			{
				GSM_USART_RX_STA|=1<<15;
			} 
		}
		USART_ClearITPendingBit(GSM_USARTx, USART_IT_RXNE);
	}  				 											 
}   
// TIM3中断处理
void TIM3_IRQHandler(void)
{ 	
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)
	{	 			   
		GSM_USART_RX_STA|=1<<15;
		TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
		TIM_Cmd(TIM3, DISABLE); 
	}	    
}
/**
  * @brief  GSM 10ms定时器初始化
  * @param  None
  * @retval None
  * @note   用于检测GSM串口接收状态
*/
void GSM_TIM3_Init(void)
{	
	NVIC_InitTypeDef NVIC_InitStructure;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	
	TIM_DeInit(TIM3);
	TIM_TimeBaseStructure.TIM_Period = 1000-1; 
	TIM_TimeBaseStructure.TIM_Prescaler = 7200-1; 
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; 
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; 
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);
	TIM_Cmd(TIM3,DISABLE);

	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}
/**
  * @brief  GSM 串口初始化
  * @param  bound: 波特率
  * @retval None
  * @note   None
*/
void GSM_USART_Init(uint32_t bound)
{  
	NVIC_InitTypeDef NVIC_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;

	GSM_USART_GPIO_APBxClkCmd(GSM_USART_GPIO_CLK, ENABLE);
	GSM_USART_APBxClkCmd(GSM_USART_CLK,ENABLE);

 	USART_DeInit(GSM_USARTx); 
	//USART_TX
	GPIO_InitStructure.GPIO_Pin = GSM_USART_TX_GPIO_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GSM_USART_TX_GPIO_PORT, &GPIO_InitStructure);
	//USART_RX
	GPIO_InitStructure.GPIO_Pin = GSM_USART_RX_GPIO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GSM_USART_RX_GPIO_PORT, &GPIO_InitStructure);
	
	USART_InitStructure.USART_BaudRate = bound;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	
	USART_Init(GSM_USARTx, &USART_InitStructure);
	
	USART_Cmd(GSM_USARTx, ENABLE);
	USART_ITConfig(GSM_USARTx, USART_IT_RXNE, ENABLE);
	
	NVIC_InitStructure.NVIC_IRQChannel = GSM_USART_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);	//
	
	GSM_USART_RX_STA=0;
}

