#include <stdio.h>
#include "GSM_func.h"

int main()
{
	char mes[] = "this is message";
	uint8_t recMesID = 0;
	GSM_Init(); // 初始化
	GSM_sendMessage(mes);
	while(1) {
		// 接收到短信send:1
		if((recMesID = GSM_waitMessage()) != 0) {
			char val = GSM_readMessage(recMesID); // val = 1
			printf("rec mes val:%c\n", val);
		}
	}
}