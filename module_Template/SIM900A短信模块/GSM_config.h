#ifndef __GSM_CONFIG_H
#define __GSM_CONFIG_H	 

#include "main.h"

#define GSM_USART_MAX_RECV_LEN		128		//	接收缓存大小
#define GSM_USART_MAX_SEND_LEN		32		//  发送缓存大小
#define GSM_USART_RX_EN 			1		//  接收使能标志

#define  GSM_USARTx 				USART2

extern char  GSM_USART_RX_BUF[GSM_USART_MAX_RECV_LEN]; 
extern char  GSM_USART_TX_BUF[GSM_USART_MAX_SEND_LEN];
extern volatile uint16_t GSM_USART_RX_STA; // 接收数据状态

void GSM_USART_IRQHandler(void);
void TIM3_IRQHandler(void);
void GSM_TIM3_Init(void);
void GSM_USART_Init(uint32_t bound);

#endif
