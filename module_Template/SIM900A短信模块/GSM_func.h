#ifndef __GSM_FUNC_H
#define __GSM_FUNC_H

#include "main.h"


char GSM_send_cmd(char *cmd,char *ack,uint16_t waittime);
int stringToInt(char *str);
void GSM_sendMessage(char *message);
uint8_t GSM_waitMessage(void);
char GSM_readMessage(uint8_t addrNum);
void GSM_Init(void);

#endif
