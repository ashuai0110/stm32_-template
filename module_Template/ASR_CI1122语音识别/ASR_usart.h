#ifndef __ASR_USART_AS_H
#define	__ASR_USART_AS_H

#include "main.h"

typedef struct {
	void (* Init)(void);
	void (* SendByte)(char ch);
	void (* SendString)(char *str);
	void (* ASR_Handle)(void);
}ASRUsartClassStruct;

extern ASRUsartClassStruct ASRUsartClass;
#endif
