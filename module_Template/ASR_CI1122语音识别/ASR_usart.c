#include "main.h"

static void USART_Config(void);
static void Usart_SendByte(char ch);
static void Usart_SendString(char *str);
static void VoiceRecData_handle(void);
static void voiceUsartString(char* str, char head, char value1, char value2);

ASRUsartClassStruct ASRUsartClass = {
	.Init = USART_Config,
	.SendByte = Usart_SendByte,
	.SendString = Usart_SendString,
	.ASR_Handle = VoiceRecData_handle
};

char voiceUsartStringData[6];
static uint8_t voiceUsartRecData = 0;

#if 0
#define  ASR_USARTx                   USART1
#define  ASR_USART_CLK                RCC_APB2Periph_USART1
#define  ASR_USART_APBxClkCmd         RCC_APB2PeriphClockCmd
#define  ASR_USART_BAUDRATE           115200

#define  ASR_USART_GPIO_CLK           (RCC_APB2Periph_GPIOA)
#define  ASR_USART_GPIO_APBxClkCmd    RCC_APB2PeriphClockCmd
    
#define  ASR_USART_TX_GPIO_PORT       GPIOA   
#define  ASR_USART_TX_GPIO_PIN        GPIO_Pin_9
#define  ASR_USART_RX_GPIO_PORT       GPIOA
#define  ASR_USART_RX_GPIO_PIN        GPIO_Pin_10

#define  ASR_USART_IRQ                USART1_IRQn
#define  ASR_USART_IRQHandler         USART1_IRQHandler

#else
#define  ASR_USARTx                   USART2
#define  ASR_USART_CLK                RCC_APB1Periph_USART2
#define  ASR_USART_APBxClkCmd         RCC_APB1PeriphClockCmd
#define  ASR_USART_BAUDRATE           115200
                                        
#define  ASR_USART_GPIO_CLK           (RCC_APB2Periph_GPIOA)
#define  ASR_USART_GPIO_APBxClkCmd    RCC_APB2PeriphClockCmd
                                        
#define  ASR_USART_TX_GPIO_PORT       GPIOA   
#define  ASR_USART_TX_GPIO_PIN        GPIO_Pin_2
#define  ASR_USART_RX_GPIO_PORT       GPIOA
#define  ASR_USART_RX_GPIO_PIN        GPIO_Pin_3
                                        
#define  ASR_USART_IRQ                USART2_IRQn
#define  ASR_USART_IRQHandler         USART2_IRQHandler
#endif

/**
 * @brief  串口初始化
 * @param  None
 * @retval None
 * @note   None
 */
static void USART_Config(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;

	ASR_USART_GPIO_APBxClkCmd(ASR_USART_GPIO_CLK, ENABLE);
	
	ASR_USART_APBxClkCmd(ASR_USART_CLK, ENABLE);

	GPIO_InitStructure.GPIO_Pin = ASR_USART_TX_GPIO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(ASR_USART_TX_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = ASR_USART_RX_GPIO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(ASR_USART_RX_GPIO_PORT, &GPIO_InitStructure);
	
	USART_InitStructure.USART_BaudRate = ASR_USART_BAUDRATE;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(ASR_USARTx, &USART_InitStructure);	

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	NVIC_InitStructure.NVIC_IRQChannel = ASR_USART_IRQ;  
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1; 
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0; 
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	USART_Cmd(ASR_USARTx, ENABLE);
	
	USART_ITConfig(ASR_USARTx, USART_IT_RXNE, ENABLE);   
}
/**
 * @brief  ASR串口中断
 * @param  None
 * @retval None
 */ 
void ASR_USART_IRQHandler(void)
{      
	if(USART_GetITStatus(ASR_USARTx, USART_IT_RXNE) != RESET)
	{
		voiceUsartRecData = USART_ReceiveData(ASR_USARTx);
#ifdef DEBUG_printf
		printf("ASR ID : %d\n", voiceUsartRecData);
#endif
	}  				 											 
}
/**
  * @brief  语音数据处理
  * @param  None
  * @retval None
  * @note   接收语音模块传回的数据，进行指定操作
	*/
static void VoiceRecData_handle(void)
{
	if(voiceUsartRecData != 0)
	{
		switch(voiceUsartRecData)
		{
			case 0x5: // 打开窗帘
				if(!AllFlag.window_CurrentSW_flag) AllFlag.window_SW_flag = 1;
				break;
			case 0x6: // 关闭窗帘
				if(AllFlag.window_CurrentSW_flag) AllFlag.window_SW_flag = 2;
				break;
			case 0x7: // 打开灯光
				AllFlag.LED_SW_flag = 1;
				break;
			case 0x8: // 关闭灯光
				AllFlag.LED_SW_flag = 0;
				break;
			case 0x9: // 打开风扇
				AllFlag.AutoCtrl_openTime = 0;
				AllFlag.AutoCtrl_flag = 1;
				GPIOClass.Set(FAN);
				break;
			case 0xA: // 关闭风扇
				AllFlag.AutoCtrl_openTime = 0;
				AllFlag.AutoCtrl_flag = 1;
				GPIOClass.Reset(FAN);
				break;
			case 0xB: // 打开加湿器
				AllFlag.AutoCtrl_openTime = 0;
				AllFlag.AutoCtrl_flag = 1;
				GPIOClass.Set(RELAY);
				break;
			case 0xC: // 关闭加湿器
				AllFlag.AutoCtrl_openTime = 0;
				AllFlag.AutoCtrl_flag = 1;
				GPIOClass.Reset(RELAY);
				break;
			case 0xD: // 当前温度
				voiceUsartString(voiceUsartStringData, 'A', DHT11Data.temp_int, DHT11Data.humi_int);
				Usart_SendString(voiceUsartStringData);
			case 0xE: // 当前湿度
				voiceUsartString(voiceUsartStringData, 'A', DHT11Data.temp_int, DHT11Data.humi_int);
				Usart_SendString(voiceUsartStringData);
				break;
		}
		voiceUsartRecData = 0;
	}
}
/**
  * @brief  语音串口协议 拼接字符串
	* @param  *str:字符串地址
	* @param  head:帧头
	* @param  value1:数据值
	* @param  value2:数据值
  * @retval None
  * @note   None
*/
static void voiceUsartString(char* str, char head, char value1, char value2)
{
	str[0] = head;
	str[1] = value1/10 + '0';
	str[2] = value1%10 + '0';
	str[3] = value2/10 + '0';
	str[4] = value2%10 + '0';
	str[5] = '\0';
}
/**
 * @brief  阻塞发送一个字节
 * @param  None
 * @retval None
 */
static void Usart_SendByte(char ch)
{
	USART_SendData(ASR_USARTx,ch);
		
	while(USART_GetFlagStatus(ASR_USARTx, USART_FLAG_TXE) == RESET);	
}
/**
 * @brief  阻塞发送字符串
 * @param  None
 * @retval None
 */
static void Usart_SendString(char *str)
{
	unsigned int k=0;
  do 
  {
		Usart_SendByte(*(str + k) );
		k++;
  }while(*(str + k)!='\0');
  
  while(USART_GetFlagStatus(ASR_USARTx,USART_FLAG_TC)==RESET);
}
