#include <stdio.h>
#include "Encoder.h"

int main()
{
	int encoder = 0;
	encoder_init_TIM3(); // 初始化
	while(1) {
		// 间隔100ms读取一次霍尔编码
		encoder = encoder_read(TIM3_Encoder);
		printf("hall sensor code:%d\n", encoder);
		delayMs(100);
	}
}