#include "Encoder.h"


// TIM3 CH1 GPIO DEFINE
#define TIM3_CH1_GPIO_PORT              GPIOA
#define TIM3_CH1_GPIO_CLK               RCC_APB2Periph_GPIOA
#define TIM3_CH1_GPIO_PIN               GPIO_Pin_6
// TIM3 CH2 GPIO DEFINE
#define TIM3_CH2_GPIO_PORT              GPIOA
#define TIM3_CH2_GPIO_CLK               RCC_APB2Periph_GPIOA
#define TIM3_CH2_GPIO_PIN               GPIO_Pin_7

// TIM4 CH1 GPIO DEFINE
#define TIM4_CH1_GPIO_PORT              GPIOB
#define TIM4_CH1_GPIO_CLK               RCC_APB2Periph_GPIOB
#define TIM4_CH1_GPIO_PIN               GPIO_Pin_6
// TIM4 CH2 GPIO DEFINE
#define TIM4_CH2_GPIO_PORT              GPIOB
#define TIM4_CH2_GPIO_CLK               RCC_APB2Periph_GPIOB
#define TIM4_CH2_GPIO_PIN               GPIO_Pin_7

void TIM3_IRQHandler(void)
{
	TIM3->SR&=~(1<<0);
}

void TIM4_IRQHandler(void)
{
	TIM4->SR&=~(1<<0);
}

void encoder_init_TIM3(void)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;  
	TIM_ICInitTypeDef TIM_ICInitStructure;  
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_APB2PeriphClockCmd(TIM3_CH1_GPIO_CLK | TIM3_CH2_GPIO_CLK, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	
	// 若引脚使用了重映射 切记增加重映射使能函数和开启AFIO
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Pin = TIM3_CH1_GPIO_PIN;
	GPIO_Init(TIM3_CH1_GPIO_PORT, &GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = TIM3_CH2_GPIO_PIN;
	GPIO_Init(TIM3_CH2_GPIO_PORT, &GPIO_InitStructure);

	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Prescaler = 0; // 预分频器 
	TIM_TimeBaseStructure.TIM_Period = 0xFFFF; //设定计数器自动重装值
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;//选择时钟分频：不分频
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;////TIM向上计数  
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);
	
	TIM_EncoderInterfaceConfig(TIM3, TIM_EncoderMode_TI12, TIM_ICPolarity_Rising, TIM_ICPolarity_Rising);//使用编码器模式3
	TIM_ICStructInit(&TIM_ICInitStructure);
	TIM_ICInitStructure.TIM_ICFilter = 10;
	TIM_ICInit(TIM3, &TIM_ICInitStructure);
	
	TIM_ClearFlag(TIM3, TIM_FLAG_Update);
	TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
	//Reset counter
	TIM_SetCounter(TIM3, 0);
	TIM_Cmd(TIM3, ENABLE); 
}

void encoder_init_TIM4(void)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;  
	TIM_ICInitTypeDef TIM_ICInitStructure;  
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_APB2PeriphClockCmd(TIM4_CH1_GPIO_CLK | TIM4_CH2_GPIO_CLK, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Pin = TIM4_CH1_GPIO_PIN;
	GPIO_Init(TIM4_CH1_GPIO_PORT, &GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = TIM4_CH2_GPIO_PIN;
	GPIO_Init(TIM4_CH2_GPIO_PORT, &GPIO_InitStructure);
	
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Prescaler = 0; // 预分频器 
	TIM_TimeBaseStructure.TIM_Period = 0xFFFF; //设定计数器自动重装值
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;//选择时钟分频：不分频
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;////TIM向上计数  
	TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);
	
	TIM_EncoderInterfaceConfig(TIM4, TIM_EncoderMode_TI12, TIM_ICPolarity_Rising, TIM_ICPolarity_Rising);//使用编码器模式3
	TIM_ICStructInit(&TIM_ICInitStructure);
	TIM_ICInitStructure.TIM_ICFilter = 10;
	TIM_ICInit(TIM4, &TIM_ICInitStructure);
	
	TIM_ClearFlag(TIM4, TIM_FLAG_Update);
	TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);
	//Reset counter
	TIM_SetCounter(TIM4, 0);
	TIM_Cmd(TIM4, ENABLE); 
}

// 读取编码器数据 读取后清零(TIM3->CNT = 0;)
int encoder_read(uint8_t who)
{
	int Encoder_TIM;
	
	switch(who) {
		case TIM3_Encoder: Encoder_TIM = (short)TIM3->CNT; TIM3->CNT = 0; break;
		case TIM4_Encoder: Encoder_TIM = (short)TIM4->CNT; TIM4->CNT = 0; break;
		default: Encoder_TIM = 0; break;
	}
	
	return Encoder_TIM;
}
