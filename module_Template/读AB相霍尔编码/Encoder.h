#ifndef __ENCODER_AS_H
#define __ENCODER_AS_H


typedef enum {
	TIM3_Encoder = 0,
	TIM4_Encoder
}EncoderEnum_t;

void encoder_init_TIM3(void);
void encoder_init_TIM4(void);
int encoder_read(unsigned char who);

#endif
