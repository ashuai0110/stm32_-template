#ifndef _RXFIFO_H
#define _RXFIFO_H

#define CAN_RXBUFFER_SIZE           (128U)
#define NVIC_INTERRPUT_DISABLE      // 自行定义
#define NVIC_INTERRPUT_ENABLE       // 自行定义
    
typedef struct {
    uint32_t id;
    uint8_t  dlc;
    uint8_t  data[8];
}can_message_t;

typedef struct {
    uint16_t WriteIndex;        /*The index of the frame that is currently receiving data*/
    uint16_t ReadIndex;         /*Complete frame, read data start index*/
    uint16_t GotMsgNum;         /*The number of complete frames received*/
    uint16_t BufferSize;        /*The maximum number of frames the buffer can hold*/
    can_message_t *pBuffer;     /*Buffer first address*/
}RxFIFOCtrl_t;

extern RxFIFOCtrl_t RxFIFOCtrl;

void pushRxFIFO(can_message_t can_message);
int32_t popRxFIFO(can_message_t **can_message, uint8_t msglen);
    

#endif /* _RXFIFO_H */
