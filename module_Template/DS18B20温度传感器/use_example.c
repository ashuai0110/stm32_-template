#include <stdio.h>
#include "DS18B20.h"

int main()
{
	float tem;
	DS18B20.Init(); // 初始化
	while(1) {
		tem = DS18B20.GetTemp_SkipRom(); // 获取温湿度
		printf("tem:%f\n", tem);
		delayMs(1000);
	}
}