#ifndef __Ds18b20_H_
#define __Ds18b20_H_

#include "main.h"

typedef struct
{
	uint8_t (*Init)(void);
	void (*ReadID)(uint8_t*);
	float (*GetTemp_SkipRom)(void);
	float (*GetTemp_MatchRom)(uint8_t*);
}DS18B20_t;

extern DS18B20_t  DS18B20;

#endif
