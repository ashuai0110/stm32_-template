#ifndef __SG90_H
#define __SG90_H

#include "main.h"

typedef struct {
	void (* Init)(void);
	void (* Angle)(float angle);
	void (* Angle2)(float angle);
} SG90ClassStruct;

extern SG90ClassStruct SG90Class;

#endif
