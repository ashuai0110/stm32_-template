#include <stdio.h>
#include "SG90.h"

int main()
{
	SG90Class.Init(); // 初始化
	while(1) {
		SG90Class.Angle(0); // 旋转0度
		SG90Class.Angle2(0); // if you use TIM_CH2
		delayMs(1000);
		SG90Class.Angle(45); // 旋转45度
		SG90Class.Angle2(180); // if you use TIM_CH2
		delayMs(1000);
	}
}