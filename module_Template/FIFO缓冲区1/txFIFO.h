#ifndef _TXFIFO_H
#define _TXFIFO_H

#define CAN_TXBUFFER_SIZE           (128U)
#define CAN_TXREPEAT_NUM            (1U)
    
typedef struct {
    uint32_t id;
    uint8_t  dlc;
    uint8_t  data[8];
}can_message_t;

typedef struct {
    uint16_t index;
    uint16_t usedSize;
    uint16_t totalSize;
    uint16_t usedRepeat;
    uint16_t totalRepeat;
    can_message_t *pBuffer;
} TxFIFOCtrl_t;

uint8_t pushTxFIFO(TxFIFOCtrl_t *ctrl, can_message_t msg);
void txFIFOTask(TxFIFOCtrl_t *ctrl);
    

#endif /* _TXFIFO_H */
