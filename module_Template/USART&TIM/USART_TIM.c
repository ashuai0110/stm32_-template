#include "main.h"	 

#if 1
#define  USART_TIMx                   USART1
#define  USART_TIM_CLK                RCC_APB2Periph_USART1
#define  USART_TIM_APBxClkCmd         RCC_APB2PeriphClockCmd
#define  USART_TIM_BAUDRATE           9600

#define  USART_TIM_GPIO_CLK           (RCC_APB2Periph_GPIOA)
#define  USART_TIM_GPIO_APBxClkCmd    RCC_APB2PeriphClockCmd
    
#define  USART_TIM_TX_GPIO_PORT       GPIOA   
#define  USART_TIM_TX_GPIO_PIN        GPIO_Pin_9
#define  USART_TIM_RX_GPIO_PORT       GPIOA
#define  USART_TIM_RX_GPIO_PIN        GPIO_Pin_10

#define  USART_TIM_IRQ                USART1_IRQn
#define  USART_TIM_IRQHandler         USART1_IRQHandler
#else
#define  USART_TIMx                   USART2
#define  USART_TIM_CLK                RCC_APB1Periph_USART2
#define  USART_TIM_APBxClkCmd         RCC_APB1PeriphClockCmd
#define  USART_TIM_BAUDRATE           115200
                                        
#define  USART_TIM_GPIO_CLK           (RCC_APB2Periph_GPIOA)
#define  USART_TIM_GPIO_APBxClkCmd    RCC_APB2PeriphClockCmd
                                        
#define  USART_TIM_TX_GPIO_PORT       GPIOA   
#define  USART_TIM_TX_GPIO_PIN        GPIO_Pin_2
#define  USART_TIM_RX_GPIO_PORT       GPIOA
#define  USART_TIM_RX_GPIO_PIN        GPIO_Pin_3
                                        
#define  USART_TIM_IRQ                USART2_IRQn
#define  USART_TIM_IRQHandler         USART2_IRQHandler
#endif

// 串口接收和发送缓存区
char USART_TIM_RX_BUF[USART_TIM_MAX_RECV_LEN]; 				
char USART_TIM_TX_BUF[USART_TIM_MAX_SEND_LEN]; 				

// [14:0]: 接收字节数 [15]: 接收完成标志
volatile uint16_t USART_TIM_RX_STA = 0;   	

// WIFI串口中断处理
void USART_TIM_IRQHandler(void)
{
	uint8_t res;	      
	if(USART_GetITStatus(USART_TIMx, USART_IT_RXNE) != RESET)
	{	 
		res =USART_ReceiveData(USART_TIMx);		 
		if((USART_TIM_RX_STA&(1<<15))==0)
		{ 
			if(USART_TIM_RX_STA<USART_TIM_MAX_RECV_LEN)
			{
				TIM_SetCounter(TIM3,0);
				if(USART_TIM_RX_STA==0) 
				{
					TIM_Cmd(TIM3,ENABLE);
				}
				USART_TIM_RX_BUF[USART_TIM_RX_STA++]=res;	
			}else 
			{
				USART_TIM_RX_STA|=1<<15;
			} 
		}
	}  				 											 
}   
/* 在别处复现 接收到数据的处理函数
void USART_Rec_Handle(void)
{
	// 接收到一次数据了
	if(USART_TIM_RX_STA&0X8000)		
	{ 
		// 添加结束符
		USART_TIM_RX_BUF[USART_TIM_RX_STA&0X7FFF]=0;
#ifdef DEBUG_printf
		printf("%s\n",USART_TIM_RX_BUF);
#endif
		// user handle
		
		// 清空缓存区
		memset(USART_TIM_RX_BUF, 0, USART_TIM_MAX_RECV_LEN);
		// 清除接收标志
		USART_TIM_RX_STA = 0;
	} 
}
*/
// TIM3中断处理
void TIM3_IRQHandler(void)
{ 	
	if(TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)
	{	 			   
		USART_TIM_RX_STA|=1<<15;
		TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
		TIM_Cmd(TIM3, DISABLE); 
	}	    
}
/**
  * @brief  10ms定时器初始化
	* @param  None
  * @retval None
  * @note   用于检测串口接收状态
*/
void USART_TIM_TIM_Init(void)
{	
	NVIC_InitTypeDef NVIC_InitStructure;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	
	TIM_DeInit(TIM3);
	TIM_TimeBaseStructure.TIM_Period = 1000-1; 
	TIM_TimeBaseStructure.TIM_Prescaler = 7200-1; 
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; 
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; 
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);
	TIM_Cmd(TIM3,DISABLE);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}
/**
  * @brief  串口初始化
	* @param  None
  * @retval None
  * @note   None
*/
void USART_TIM_USART_Init(void)
{  
	NVIC_InitTypeDef NVIC_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;

	USART_TIM_GPIO_APBxClkCmd(USART_TIM_GPIO_CLK, ENABLE);
	USART_TIM_APBxClkCmd(USART_TIM_CLK,ENABLE);

 	USART_DeInit(USART_TIMx); 
	//USART_TX
  GPIO_InitStructure.GPIO_Pin = USART_TIM_TX_GPIO_PIN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(USART_TIM_TX_GPIO_PORT, &GPIO_InitStructure);
  //USART_RX
  GPIO_InitStructure.GPIO_Pin = USART_TIM_RX_GPIO_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(USART_TIM_RX_GPIO_PORT, &GPIO_InitStructure);
	
	USART_InitStructure.USART_BaudRate = USART_TIM_BAUDRATE;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	
	USART_Init(USART_TIMx, &USART_InitStructure);
	
	USART_Cmd(USART_TIMx, ENABLE);
  USART_ITConfig(USART_TIMx, USART_IT_RXNE, ENABLE);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	NVIC_InitStructure.NVIC_IRQChannel = USART_TIM_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	USART_TIM_RX_STA=0;
}

