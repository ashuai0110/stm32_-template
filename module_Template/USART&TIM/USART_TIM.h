#ifndef __USART_TIM_AS_H
#define __USART_TIM_AS_H	 

#include "main.h" 	   

#define USART_TIM_MAX_RECV_LEN		128		//	接收缓存大小
#define USART_TIM_MAX_SEND_LEN		64		//  发送缓存大小
#define USART_TIM_RX_EN 					1			//  接收使能标志

#define  USART_TIMx                   USART1

extern char  USART_TIM_RX_BUF[USART_TIM_MAX_RECV_LEN]; 
extern char  USART_TIM_TX_BUF[USART_TIM_MAX_SEND_LEN];
extern volatile uint16_t USART_TIM_RX_STA; // 接收数据状态

void USART_TIM_TIM_Init(void);
void USART_TIM_USART_Init(void);

#endif
