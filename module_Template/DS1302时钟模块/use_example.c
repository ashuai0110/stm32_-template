#include <stdio.h>
#include "DS1302.h"

int main()
{
	// 2023年4月9日9点0分0秒星期日
	uint8_t initTime[8] = {0x20, 0x23, 0x04, 0x09, 0x09, 0x00, 0x00, 0x07};
	DS1302TimeStruct getTime;
	DS1302Class.Init(); // 初始化
	DS1302Class.SetTime(initTime); // 写入时间
	while(1) {
		DS1302Class.GetTime(&getTime); // 获取时间
		printf("%d-%d-%d %d:%d:%d\n", getTime.year, getTime.month, getTime.day, getTime.hour, getTime.minute, getTime.second);
		delayMs(1000);
	}
}