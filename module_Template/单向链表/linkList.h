#ifndef __LINKLIST_H__
#define __LINKLIST_H__

#ifdef __cplusplus
extern "C" {
#endif
	

typedef struct linkList{
	uint16_t data;
  struct linkList *next;
} linkListStruct;

linkListStruct *linkListCreate(uint16_t n);
linkListStruct *linkListAppend(linkListStruct *list, uint16_t data);
uint32_t linkListSum(linkListStruct *list, uint8_t deleteNum, uint16_t count, float ratio);

#ifdef __cplusplus
}
#endif
#endif /* __LINKLIST_H__ */

