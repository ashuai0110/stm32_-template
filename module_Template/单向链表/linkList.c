#include "linkList.h"

/**
  * @brief  创建链表 数据默认为0
	* @param	n: 节点数量
  * @retval 链表头节点
  */
linkListStruct *linkListCreate(uint16_t n)
{
    linkListStruct *head, *end;
	
    head = (linkListStruct *)Mymalloc.Malloc(sizeof(linkListStruct));
    head->data = 0;
    end = head;
    for(uint16_t i = 1; i < n; i++) {
        end->next = (linkListStruct *)Mymalloc.Malloc(sizeof(linkListStruct));
        end->next->data = 0;
        end = end->next;
    }
    end->next = NULL;

    return head;
}
/**
  * @brief  链表追加创建节点
	* @param	list: 链表尾结点
	* @param	data: 数据
  * @retval 链表尾节点
  */
linkListStruct *linkListAppend(linkListStruct *list, uint16_t data)
{
    list->next = (linkListStruct *)Mymalloc.Malloc(sizeof(linkListStruct));
    list->next->data = data;
    list->next->next = NULL;
	
    return list->next;
}
/**
  * @brief  链表数据求和
	* @param	list: 链表头节点
	* @param	deleteNum: 舍弃的头尾数据量
	* @param	count: 数据总量
	* @param	ratio: 乘法系数
  * @retval 链表头节点地址
  */
uint32_t linkListSum(linkListStruct *list, uint8_t deleteNum, uint16_t count, float ratio)
{
    uint32_t sum = 0;
    uint16_t i = 0;
	
    while(i < deleteNum && list->next != NULL) {
        list = list->next;
        i++;
    }
    count -= deleteNum;
    sum += (uint32_t)(list->data * ratio);
		printf("link list data %d : %d\n", i, (uint32_t)(list->data * ratio));
    i += 1;
    while(i < count && list->next != NULL) {
        list = list->next;
        sum += list->data * ratio;
				printf("link list data %d : %d\n", i, (uint32_t)(list->data * ratio));
        i++;
    }
		printf("link list sum : %d, count : %d\n", sum, count);
    return sum;
}
