#include <stdio.h>
#include "ADXL345.h"

int main()
{
	float angleX, angleY, angleZ;
	ADXL345_init(); // 初始化
	while(1) {
		delayMs(1000);
		get_angle(&angleX, &angleY, &angleZ); // 获取三轴偏移角度
		printf("angle X:%f  Y:%f  Z:%f\n", angleX, angleY, angleZ);
	}
}