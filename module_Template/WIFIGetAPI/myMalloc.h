#ifndef __MYMALLOC_H
#define __MYMALLOC_H

#include "main.h"
 
//内存管理控制器
struct _m_mallco_dev
{
	void (*init)(void);					//初始化
	uint8_t   (*perused)(void);		  	    	//内存使用率
	uint8_t 	  *membase;					//内存池
	uint16_t   *memmap; 					//内存管理状态表
	uint8_t     memrdy; 						//内存管理是否就绪
};
extern struct _m_mallco_dev mallco_dev;	 //在mallco.c里面定义

void mem_init(void);
void myfree(void *ptr);
void *mymalloc(uint32_t size);
void *myrealloc(void *ptr,uint32_t size);
#endif


