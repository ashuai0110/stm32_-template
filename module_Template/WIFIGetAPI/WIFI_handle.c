#include "main.h"
//#include "WIFI_config.h"
//#include "WIFI_func.h"
//#include "cJSON.h"

static uint8_t weatherCJSON_handle(char s[], WIFIWeatherStruct *WIFIWeather1);
static uint8_t timeCJSONToString(char s[], uint8_t *timeBuff1, uint8_t hexOrDec1);
static void timeJsonTotimeInt(char *str1, uint8_t *timeBuff2, uint8_t hexOrDec2);
static int stringToInt(char *str);


/**
  * @brief  WIFI初始化
  * @param  None
  * @retval None
  * @note   None
	*/
void WIFI_Init(void)
{
	mem_init();
	WIFI_USART_Init(115200);
	WIFI_TIM3_Init();
}
/**
  * @brief  WIFI联网
  * @param  None
  * @retval None
  * @note   None
	*/
void WIFI_Connect(void)
{
	WIFI_RESET_GPIO_Congfig();
	ESP8266_clear();
	ESP8266_quit_trans(); // 关闭透传与TCP模式
	Connect_wifi();
}
/**
  * @brief  WIFI联网获取实时时间数据
	* @param  timeBuff: 时间存放地址
	* @param  hexOrDec: 转化为十六进制表示还是十进制表示 输入16或10
	* @retval 0: 解析成功
  * @note   None
	*/
uint8_t WIFI_RealTime_handle(uint8_t *timeBuff, uint8_t hexOrDec)
{
	uint8_t result = 1;
	Connect_time_port();
	result = timeCJSONToString(WIFI_USART_RX_BUF, timeBuff, hexOrDec);
	ESP8266_quit_trans(); // 必须放在解析数据之后
	return result;
}
/**
  * @brief  WIFI联网获取实时天气数据
  * @param  None
  * @retval None
  * @note   None
	*/
uint8_t WIFI_realWeather_handle(WIFIWeatherStruct *WIFIWeather1)
{
	uint8_t result = 1;
	Connect_wea_port();
	result = weatherCJSON_handle(WIFI_USART_RX_BUF, WIFIWeather1);
	ESP8266_quit_trans(); // 必须放在解析数据之后
	return result;
}
/**
  * @brief  WIFI天气CJSON数据处理
  * @param  None
  * @retval None
  * @note   将CJSON数据转换为可用数据格式
	*/
static uint8_t weatherCJSON_handle(char s[], WIFIWeatherStruct *WIFIWeather2)
{
	uint8_t result = 1;
	char t[] ="[";
	char r[] ="]";
	cJSON *res,*location, *tq ,*qw, *name, *now;
	cJSON *cjson;
	Delete_char(s,t[0]);
	Delete_char(s,r[0]);
	cjson = cJSON_Parse(s);
	if(cjson)
	{
		res = cJSON_GetObjectItem(cjson, "results");
		location = cJSON_GetObjectItem(res, "location");
		name = cJSON_GetObjectItem(location, "name");
		now = cJSON_GetObjectItem(res, "now");
		tq = cJSON_GetObjectItem(now, "code");
		qw = cJSON_GetObjectItem(now, "temperature");
		WIFIWeather2->weatherCode = stringToInt(tq->valuestring);
		WIFIWeather2->weatherTemp[0] = *(qw->valuestring);
		WIFIWeather2->weatherTemp[1] = *((qw->valuestring) + 1);
		WIFIWeather2->weatherTemp[2] = '\0';
		if(WIFIWeather2->weatherTemp[0] < '0') WIFIWeather2->tempNum = 0;
		else WIFIWeather2->tempNum = stringToInt(qw->valuestring);
		result = 0;
#ifdef DEBUG_printf
	printf("cityName is %s\n",name->valuestring);
	printf("tianqi is %s\n",tq->valuestring);
	printf("tem is %s\n",qw->valuestring);
#endif
	}
	cJSON_Delete(cjson);
	myfree(s);
	return result;
}
/**
  * @brief  时间json转string
	* @param  s[]:json数据
	* @param  timeBuff1: 时间存放地址
	* @param  hexOrDec1: 转化为十六进制表示还是十进制表示 输入16或10
	* @retval 0: 解析成功
  * @note   None
*/
static uint8_t timeCJSONToString(char s[], uint8_t *timeBuff1, uint8_t hexOrDec1)
{
	uint8_t result = 1;
	cJSON *res, *time, *week;
	cJSON *cjson;
	cjson = cJSON_Parse(s);
	if(cjson)
	{
		res = cJSON_GetObjectItem(cjson, "result");
		time = cJSON_GetObjectItem(res, "datetime_1");
		week = cJSON_GetObjectItem(res, "week_1");
		if(stringToInt(week->valuestring) == 0) {
			WIFITimeData[7] = 0x07;
		} else {
			WIFITimeData[7] = stringToInt(week->valuestring);
		}
		// 解析为int方便初始化DS1302
		timeJsonTotimeInt(time->valuestring, timeBuff1, hexOrDec1);
		result = 0;
#ifdef DEBUG_printf
		printf("WIFI time: datetime is %s; week is %s\n", time->valuestring, week->valuestring);
#endif
	}
	cJSON_Delete(cjson);
	myfree(s);
	return result;
}

/**
  * @brief  解析json数据中的time
	* @param  str1:字符串地址
	* @param  timeBuff2: 时间存放地址
	* @param  hexOrDec2: 转化为十六进制表示还是十进制表示 输入16或10
  * @retval None
  * @note   None
*/
static void timeJsonTotimeInt(char *str1, uint8_t *timeBuff2, uint8_t hexOrDec2)
{
	char *str = str1;
	char temp[5];
	char i = 0,j = 0;
	int timeTemp = 0;
	memset(temp, 0, sizeof(temp)/sizeof(temp[0]));
	for(j = 0; j < 6; j ++) {
		while(*str != '-' && *str != ' ' && *str != ':') {
			if(*str == '\0') break;
			temp[i] = *str;
			i++;
			str++;
		}
		timeTemp = stringToInt(temp);
		switch(j) {
			case 0:
				timeBuff2[0] = timeTemp/100/10*hexOrDec2 + timeTemp/100%10;
				timeBuff2[1] = timeTemp%100/10*hexOrDec2 + timeTemp%100%10;
				break;
			case 1:
				timeBuff2[2] = timeTemp/10*hexOrDec2 + timeTemp%10;
				break;
			case 2:
				timeBuff2[3] = timeTemp/10*hexOrDec2 + timeTemp%10;
				break;
			case 3:
				timeBuff2[4] = timeTemp/10*hexOrDec2 + timeTemp%10;
				break;
			case 4:
				timeBuff2[5] = timeTemp/10*hexOrDec2 + timeTemp%10;
				break;
			case 5:
				timeBuff2[6] = timeTemp/10*hexOrDec2 + timeTemp%10 + 2; // 加2s补时
				break;
		}
		memset(temp, 0, sizeof(temp)/sizeof(temp[0]));
		i = 0;
		str++;
	}
#ifdef DEBUG_printf
	if(hexOrDec2 == 16)
	{
		printf("timeBuff: %x%x-%x-%x %x:%x:%x %x\n", timeBuff2[0], timeBuff2[1], timeBuff2[2], timeBuff2[3], timeBuff2[4], timeBuff2[5], timeBuff2[6], timeBuff2[7]);
	}
	else if(hexOrDec2 == 10)
	{
		printf("timeBuff: %d%d-%d-%d %d:%d:%d %d\n", timeBuff2[0], timeBuff2[1], timeBuff2[2], timeBuff2[3], timeBuff2[4], timeBuff2[5], timeBuff2[6], timeBuff2[7]);
	}
	#endif
}
/**
  * @brief  string转换int
	* @param  *str:字符串地址
  * @retval 返回int型
  * @note   None
*/
static int stringToInt(char *str)
{
	char *p = str;
	int nNUM = 0;
	
	while (*p != '\0')
	{
		nNUM = nNUM * 10 + (*p - '0');
		p++;
	}
	return nNUM;
}
/**
  * @brief  根据天气代码显示天气情况
	* @param  temp:天气代码
  * @retval None
  * @note   None
*/
void tianqiDisplay(uint8_t weatherCode)
{
	uint8_t x1 = 48;
	uint8_t x2 = 64;
	uint8_t y = 4;
	if(weatherCode <= 3) {
		OLED_ShowChinese(x1,y,27); // 晴
		OLED_ShowString(x2,y,"  ",16);
	} else if(weatherCode <= 8) {
		OLED_ShowChinese(x1,y,28); // 多云
		OLED_ShowChinese(x2,y,29);
	} else if(weatherCode == 9) {
		OLED_ShowChinese(x1,y,30); // 阴
		OLED_ShowString(x2,y,"  ",16);
	} else if(weatherCode == 10) {
		OLED_ShowChinese(x1,y,36); // 阵雨
		OLED_ShowChinese(x2,y,31);
	} else if(weatherCode <= 12) {
		OLED_ShowChinese(x1,y,35); // 雷雨
		OLED_ShowChinese(x2,y,31);
	} else if(weatherCode == 13) {
		OLED_ShowChinese(x1,y,34); // 小雨
		OLED_ShowChinese(x2,y,31);
	} else if(weatherCode == 14) {
		OLED_ShowChinese(x1,y,33); // 中雨
		OLED_ShowChinese(x2,y,31);
	} else if(weatherCode <= 19) {
		OLED_ShowChinese(x1,y,32); // 大雨
		OLED_ShowChinese(x2,y,31);
	} else if(weatherCode <= 21) {
		OLED_ShowChinese(x1,y,36); // 阵雪
		OLED_ShowChinese(x2,y,39);
	} else if(weatherCode == 22) {
		OLED_ShowChinese(x1,y,34); // 小雪
		OLED_ShowChinese(x2,y,39);
	} else if(weatherCode == 23) {
		OLED_ShowChinese(x1,y,33); // 中雪
		OLED_ShowChinese(x2,y,39);
	} else if(weatherCode <= 25) {
		OLED_ShowChinese(x1,y,32); // 大雪
		OLED_ShowChinese(x2,y,39);
	} else if(weatherCode == 30) {
		OLED_ShowChinese(x1,y,37); // 雾
		OLED_ShowString(x2,y,"  ",16);
	} else if(weatherCode == 31) {
		OLED_ShowChinese(x1,y,38); // 霾
		OLED_ShowString(x2,y,"  ",16);
	} else {
		OLED_ShowString(x1,y,"N/A ",16); // N/A
	}
}
