#ifndef __WIFI_CONFIG_H
#define __WIFI_CONFIG_H	 

#include "main.h" 	   

#define WIFI_USART_MAX_RECV_LEN		512		//	接收缓存大小
#define WIFI_USART_MAX_SEND_LEN		128		//  发送缓存大小
#define WIFI_USART_RX_EN 					1			//  接收使能标志

#define  WIFI_USARTx                   USART1

extern char  WIFI_USART_RX_BUF[WIFI_USART_MAX_RECV_LEN]; 
extern char  WIFI_USART_TX_BUF[WIFI_USART_MAX_SEND_LEN];
extern volatile uint16_t WIFI_USART_RX_STA; // 接收数据状态

void WIFI_TIM3_Init(void);
void WIFI_USART_Init(uint32_t bound);

#endif
