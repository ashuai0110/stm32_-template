#ifndef _WIFI_HANDLE_H
#define _WIFI_HANDLE_H

#include "main.h"	

//// 时间结构体
//typedef struct {
//	uint16_t year;
//	uint8_t  month;
//	uint8_t  day;
//	uint8_t  hour;
//	uint8_t  minute;
//	uint8_t  second;
//	uint8_t  week;
//} WIFITimeStruct;

// 天气结构体
typedef struct {
	uint8_t weatherCode;
	char weatherTemp[3];
	uint8_t tempNum;
}WIFIWeatherStruct;


void WIFI_Init(void);
void WIFI_Connect(void);
uint8_t WIFI_RealTime_handle(uint8_t *timeBuff, uint8_t hexOrDec);
uint8_t WIFI_realWeather_handle(WIFIWeatherStruct *WIFIWeather1);
void tianqiDisplay(uint8_t weatherCode);

#endif
