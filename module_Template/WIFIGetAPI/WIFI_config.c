#include "main.h"
//// C lib
//#include <stdarg.h>	 	 
//#include <stdio.h> 	 
//#include <string.h>	 


#if 1
#define  WIFI_USARTx                   USART1
#define  WIFI_USART_CLK                RCC_APB2Periph_USART1
#define  WIFI_USART_APBxClkCmd         RCC_APB2PeriphClockCmd
#define  WIFI_USART_BAUDRATE           115200

#define  WIFI_USART_GPIO_CLK           (RCC_APB2Periph_GPIOA)
#define  WIFI_USART_GPIO_APBxClkCmd    RCC_APB2PeriphClockCmd
    
#define  WIFI_USART_TX_GPIO_PORT       GPIOA   
#define  WIFI_USART_TX_GPIO_PIN        GPIO_Pin_9
#define  WIFI_USART_RX_GPIO_PORT       GPIOA
#define  WIFI_USART_RX_GPIO_PIN        GPIO_Pin_10

#define  WIFI_USART_IRQ                USART1_IRQn
#define  WIFI_USART_IRQHandler         USART1_IRQHandler

#else
#define  WIFI_USARTx                   USART2
#define  WIFI_USART_CLK                RCC_APB1Periph_USART2
#define  WIFI_USART_APBxClkCmd         RCC_APB1PeriphClockCmd
#define  WIFI_USART_BAUDRATE           115200
                                        
#define  WIFI_USART_GPIO_CLK           (RCC_APB2Periph_GPIOA)
#define  WIFI_USART_GPIO_APBxClkCmd    RCC_APB2PeriphClockCmd
                                        
#define  WIFI_USART_TX_GPIO_PORT       GPIOA   
#define  WIFI_USART_TX_GPIO_PIN        GPIO_Pin_2
#define  WIFI_USART_RX_GPIO_PORT       GPIOA
#define  WIFI_USART_RX_GPIO_PIN        GPIO_Pin_3
                                        
#define  WIFI_USART_IRQ                USART2_IRQn
#define  WIFI_USART_IRQHandler         USART2_IRQHandler
#endif

//	
char WIFI_USART_RX_BUF[WIFI_USART_MAX_RECV_LEN]; 				
char WIFI_USART_TX_BUF[WIFI_USART_MAX_SEND_LEN]; 				

// [14:0]: 接收字节数 [15]: 接收完成标志
volatile uint16_t WIFI_USART_RX_STA = 0;   	

// WIFI串口中断处理
void WIFI_USART_IRQHandler(void)
{
	uint8_t res;	      
	if(USART_GetITStatus(WIFI_USARTx, USART_IT_RXNE) != RESET)
	{	 
		res =USART_ReceiveData(WIFI_USARTx);		 
		if((WIFI_USART_RX_STA&(1<<15))==0)
		{ 
			if(WIFI_USART_RX_STA<WIFI_USART_MAX_RECV_LEN)
			{
				TIM_SetCounter(TIM3,0);
				if(WIFI_USART_RX_STA==0) 
				{
					TIM_Cmd(TIM3,ENABLE);
				}
				WIFI_USART_RX_BUF[WIFI_USART_RX_STA++]=res;	
			}else 
			{
				WIFI_USART_RX_STA|=1<<15;
			} 
		}
	}  				 											 
}   
// TIM3中断处理
void TIM3_IRQHandler(void)
{ 	
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)
	{	 			   
		WIFI_USART_RX_STA|=1<<15;
		TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
		TIM_Cmd(TIM3, DISABLE); 
	}	    
}
/**
  * @brief  WIFI 10ms定时器初始化
	* @param  None
  * @retval None
  * @note   用于检测WIFI串口接收状态
*/
void WIFI_TIM3_Init(void)
{	
	NVIC_InitTypeDef NVIC_InitStructure;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	
	TIM_DeInit(TIM3);
	TIM_TimeBaseStructure.TIM_Period = 1000-1; 
	TIM_TimeBaseStructure.TIM_Prescaler = 7200-1; 
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; 
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; 
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);
	TIM_Cmd(TIM3,DISABLE);

	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}
/**
  * @brief  WIFI 串口初始化
	* @param  bound: 波特率
  * @retval None
  * @note   None
*/
void WIFI_USART_Init(uint32_t bound)
{  
	NVIC_InitTypeDef NVIC_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;

	WIFI_USART_GPIO_APBxClkCmd(WIFI_USART_GPIO_CLK, ENABLE);
	WIFI_USART_APBxClkCmd(WIFI_USART_CLK,ENABLE);

 	USART_DeInit(WIFI_USARTx); 
	//USART_TX
  GPIO_InitStructure.GPIO_Pin = WIFI_USART_TX_GPIO_PIN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(WIFI_USART_TX_GPIO_PORT, &GPIO_InitStructure);
  //USART_RX
  GPIO_InitStructure.GPIO_Pin = WIFI_USART_RX_GPIO_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(WIFI_USART_RX_GPIO_PORT, &GPIO_InitStructure);
	
	USART_InitStructure.USART_BaudRate = bound;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	
	USART_Init(WIFI_USARTx, &USART_InitStructure);
	
	USART_Cmd(WIFI_USARTx, ENABLE);
  USART_ITConfig(WIFI_USARTx, USART_IT_RXNE, ENABLE);
	
	NVIC_InitStructure.NVIC_IRQChannel = WIFI_USART_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);	//
	
	WIFI_USART_RX_STA=0;
}

