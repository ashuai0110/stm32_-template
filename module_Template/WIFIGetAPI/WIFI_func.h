#ifndef __WIFI_FUNC_H
#define __WIFI_FUNC_H

#include "main.h"

void ESP8266_clear(void);
void ESP8266_quit_trans(void); 
void Delete_char(char str[],char target);
void WIFI_RESET_GPIO_Congfig(void);
void Connect_wifi(void);
void Connect_wea_port(void);
void Connect_time_port(void);

#endif



