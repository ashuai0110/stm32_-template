#ifndef __BY9301_AS_H
#define	__BY9301_AS_H

#include "main.h"

typedef struct {
	void (* init)(uint32_t);
	void (* sayFileID)(uint8_t);
	void (* setVolume)(uint8_t);
	void (* playPause)(uint8_t);
	uint8_t (* isBusy)(void);
} BY9301ClassStruct;

extern BY9301ClassStruct BY9301Class;

#endif
