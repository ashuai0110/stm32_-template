#include <stdio.h>
#include "SPI.h"
#include "RC522.h"

int main()
{
	uint8_t cardID[4] = {0};
	SPIClass.SPI1_Init(); // SPI初始化
	MFRC522_Init(); // RC522初始化
	while(1) {
		if(!RC522_cardScan(cardID)) {
			printf("card scan success, id:0x%02X%02X%02X%02X\n", cardID[0], cardID[1], cardID[2], cardID[3]);
		} else {
			printf("card scan failure\n");
		}
	}
}