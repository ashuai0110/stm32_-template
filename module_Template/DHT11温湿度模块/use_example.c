#include <stdio.h>
#include "DHT11.h"

int main()
{
	DHT11Class.Init(); // 初始化
	while(1) {
		DHT11Class.Read(&DHT11Data); // 获取温湿度
		printf("tem:%d  hum:%d\n", DHT11Data.temp_int, DHT11Data.humi_int);
		delayMs(1000);
	}
}