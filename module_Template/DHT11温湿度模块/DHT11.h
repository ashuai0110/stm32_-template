#ifndef __DHT11_AS_H
#define __DHT11_AS_H

#include "main.h"
//#include <stdio.h>

typedef struct
{
	uint8_t  humi_int;		//湿度的整数部分
	uint8_t  humi_deci;	 	//湿度的小数部分
	uint8_t  temp_int;	 	//温度的整数部分
	uint8_t  temp_deci;	 	//温度的小数部分
	uint8_t  check_sum;	 	//校验和                 
} DHT11_Data_TypeDef;

extern DHT11_Data_TypeDef DHT11Data;

typedef struct {
	void (* Init)(void);
	uint8_t (* Read)(DHT11_Data_TypeDef *DHT11_Data);
} DHT11ClassStruct;

extern DHT11ClassStruct DHT11Class;

#endif
