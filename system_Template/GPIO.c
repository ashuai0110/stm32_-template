#include "main.h"

static void GPIO_Config(void);
static void GPIO_Set(uint8_t who);
static void GPIO_Reset(uint8_t who);
static void GPIO_Toggle(uint8_t who);
static uint8_t GPIO_Get(uint8_t who);

GPIOClassStruct GPIOClass = {
	.Init = GPIO_Config,
	.Set = GPIO_Set,
	.Reset = GPIO_Reset,
	.Toggle = GPIO_Toggle,
	.Read = GPIO_Get
};

// LEDRun
#define LEDRun_GPIO_PORT    			GPIOB			              /* GPIO端口 */
#define LEDRun_GPIO_CLK 	    		RCC_APB2Periph_GPIOB		/* GPIO端口时钟 */
#define LEDRun_GPIO_PIN						GPIO_Pin_5			        /* 连接到SCL时钟线的GPIO */
#define LEDRun_GPIO_TOGGLE		 		LEDRun_GPIO_PORT->BSRR = ((LEDRun_GPIO_PORT->ODR & LEDRun_GPIO_PIN) << 16) | (~LEDRun_GPIO_PORT->ODR & LEDRun_GPIO_PIN)
#define LEDRun_GPIO_SET			   		LEDRun_GPIO_PORT->BSRR = (uint32_t)LEDRun_GPIO_PIN // 置1
#define LEDRun_GPIO_RESET		   		LEDRun_GPIO_PORT->BSRR = (uint32_t)LEDRun_GPIO_PIN<<16 // 置0
#define LEDRun_GPIO_READ					((LEDRun_GPIO_PORT->IDR & LEDRun_GPIO_PIN) != 0)
// FAN
#define FAN_GPIO_PORT    					GPIOB			              /* GPIO端口 */
#define FAN_GPIO_CLK 	    				RCC_APB2Periph_GPIOB		/* GPIO端口时钟 */
#define FAN_GPIO_PIN							GPIO_Pin_9			        /* 连接到SCL时钟线的GPIO */
#define FAN_GPIO_TOGGLE		 				FAN_GPIO_PORT->BSRR = ((FAN_GPIO_PORT->ODR & FAN_GPIO_PIN) << 16) | (~FAN_GPIO_PORT->ODR & FAN_GPIO_PIN)
#define FAN_GPIO_SET			   			FAN_GPIO_PORT->BSRR = (uint32_t)FAN_GPIO_PIN // 置1
#define FAN_GPIO_RESET		   			FAN_GPIO_PORT->BSRR = (uint32_t)FAN_GPIO_PIN<<16 // 置0
#define FAN_GPIO_READ							((FAN_GPIO_PORT->IDR & FAN_GPIO_PIN) != 0)

/**
  * @brief  IO配置
  * @param  None
  * @retval None
  */
static void GPIO_Config(void)
{		
	/*定义一个GPIO_InitTypeDef类型的结构体*/
	GPIO_InitTypeDef GPIO_InitStructure;
	
	/*开启LED相关的GPIO外设时钟*/
	RCC_APB2PeriphClockCmd(LEDRun_GPIO_CLK | RCC_APB2Periph_AFIO, ENABLE);
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
	GPIO_InitStructure.GPIO_Pin = LEDRun_GPIO_PIN;	
	/*设置引脚模式为*/
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;   
	/*设置引脚速率*/   
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
	/*调用库函数，初始化GPIO*/
	GPIO_Init(LEDRun_GPIO_PORT, &GPIO_InitStructure);	
	/*关闭*/
	LEDRun_GPIO_SET;
	
	RCC_APB2PeriphClockCmd(FAN_GPIO_CLK, ENABLE);
	GPIO_InitStructure.GPIO_Pin = FAN_GPIO_PIN;	
	GPIO_Init(FAN_GPIO_PORT, &GPIO_InitStructure);	
	FAN_GPIO_SET;
}
/**
  * @brief  IO置高
  * @param  None
  * @retval None
  */
static void GPIO_Set(uint8_t who)
{
	switch(who)
	{
		case LED_Run: LEDRun_GPIO_SET;break;
		case FAN: FAN_GPIO_SET;break;
	}
}
/**
  * @brief  IO置低
  * @param  None
  * @retval None
  */
static void GPIO_Reset(uint8_t who)
{
	switch(who)
	{
		case LED_Run: LEDRun_GPIO_RESET;break;
		case FAN: FAN_GPIO_RESET;break;
	}
}
/**
  * @brief  IO翻转
  * @param  None
  * @retval None
  */
static void GPIO_Toggle(uint8_t who)
{
	switch(who)
	{
		case LED_Run: LEDRun_GPIO_TOGGLE;break;
		case FAN: FAN_GPIO_TOGGLE;break;
	}
}
/**
  * @brief  IO读取
  * @param  None
  * @retval None
  */
static uint8_t GPIO_Get(uint8_t who)
{
	uint8_t temp = 0;
	switch(who)
	{
		case LED_Run: temp = LEDRun_GPIO_READ;break;
		case FAN: temp = FAN_GPIO_READ;break;
	}
	return temp;
}
