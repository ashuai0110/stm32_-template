#ifndef __PWM_AS_H
#define __PWM_AS_H

#include "main.h"

typedef enum {
	PWM_CH1 = 0,
	PWM_CH2,
	PWM_CH3,
	PWM_CH4

} PWM_CHX_t;

void PWM_TIM2_Init(uint16_t period, uint16_t arr);
void PWM_TIM3_Init(uint16_t period, uint16_t arr);
void PWM_setCompare(TIM_TypeDef* TIMx, uint8_t CHx, uint16_t compare);

#endif
