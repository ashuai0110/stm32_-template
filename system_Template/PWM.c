#include "main.h"


// 使用TIM2哪个通道
#define USE_TIM2_CH1
//#define USE_TIM2_CH2
//#define USE_TIM2_CH3
//#define USE_TIM2_CH4

#define PWM_TIM2_GPIO_CLK               RCC_APB2Periph_GPIOA
#define PWM_TIM2_GPIO_PORT              GPIOA
#ifdef USE_TIM2_CH1
#define PWM_TIM2_GPIO_PIN               GPIO_Pin_0
#endif
#ifdef USE_TIM2_CH2
#define PWM_TIM2_GPIO_PIN2              GPIO_Pin_1
#endif
#ifdef USE_TIM2_CH3
#define PWM_TIM2_GPIO_PIN3              GPIO_Pin_2
#endif
#ifdef USE_TIM2_CH4
#define PWM_TIM2_GPIO_PIN4              GPIO_Pin_3
#endif

// 使用TIM3哪个通道
#define USE_TIM3_CH1
//#define USE_TIM3_CH2
//#define USE_TIM3_CH3
//#define USE_TIM3_CH4

#define PWM_TIM3_GPIO_CLK               RCC_APB2Periph_GPIOA
#define PWM_TIM3_GPIO_PORT              GPIOA
#ifdef USE_TIM3_CH1
#define PWM_TIM3_GPIO_PIN               GPIO_Pin_6
#endif
#ifdef USE_TIM3_CH2
#define PWM_TIM3_GPIO_PIN2              GPIO_Pin_7
#endif
#ifdef USE_TIM3_CH3
#define PWM_TIM3_GPIO_PIN3              GPIO_Pin_8
#endif
#ifdef USE_TIM3_CH4
#define PWM_TIM3_GPIO_PIN4              GPIO_Pin_9
#endif

void PWM_TIM2_Init(uint16_t period, uint16_t arr)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;

	RCC_APB2PeriphClockCmd(PWM_TIM2_GPIO_CLK | RCC_APB2Periph_AFIO, ENABLE);  //使能GPIO外设时钟使能
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE); //时钟使能
	
	//设置该引脚为复用输出功能,输出PWM脉冲波形
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;  //复用推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
#ifdef USE_TIM2_CH1
	GPIO_InitStructure.GPIO_Pin = PWM_TIM2_GPIO_PIN;
	GPIO_Init(PWM_TIM2_GPIO_PORT, &GPIO_InitStructure);
#endif
#ifdef USE_TIM2_CH2
	GPIO_InitStructure.GPIO_Pin = PWM_TIM2_GPIO_PIN2;
	GPIO_Init(PWM_TIM2_GPIO_PORT, &GPIO_InitStructure);
#endif
#ifdef USE_TIM2_CH3
	GPIO_InitStructure.GPIO_Pin = PWM_TIM2_GPIO_PIN3;
	GPIO_Init(PWM_TIM2_GPIO_PORT, &GPIO_InitStructure);
#endif
#ifdef USE_TIM2_CH4
	GPIO_InitStructure.GPIO_Pin = PWM_TIM2_GPIO_PIN4;
	GPIO_Init(PWM_TIM2_GPIO_PORT, &GPIO_InitStructure);
#endif
	
	//定时器初始化
	TIM_TimeBaseStructure.TIM_Period = period; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值
	TIM_TimeBaseStructure.TIM_Prescaler = arr; //设置用来作为TIMx时钟频率除数的预分频值
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; //设置时钟分割:TDTS = Tck_tim
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
	TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure); //根据指定的参数初始化TIMx的时间基数单位
 
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2; //选择定时器模式:TIM脉冲宽度调制模式2
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; //比较输出使能
	TIM_OCInitStructure.TIM_Pulse = 0; //设置待装入捕获比较寄存器的脉冲值
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low; //输出极性:TIM输出比较极性高
#ifdef USE_TIM2_CH1
	TIM_OC1Init(TIM2, &TIM_OCInitStructure);  //根据TIM_OCInitStruct中指定的参数初始化外设TIMx
	TIM_OC1PreloadConfig(TIM2, TIM_OCPreload_Enable);  	//CH1预装载使能   
#endif	
#ifdef USE_TIM2_CH2
	TIM_OC2Init(TIM2, &TIM_OCInitStructure);
	TIM_OC2PreloadConfig(TIM2, TIM_OCPreload_Enable);
#endif
#ifdef USE_TIM2_CH3
	TIM_OC3Init(TIM2, &TIM_OCInitStructure);
	TIM_OC3PreloadConfig(TIM2, TIM_OCPreload_Enable);
#endif
#ifdef USE_TIM2_CH4
	TIM_OC4Init(TIM2, &TIM_OCInitStructure);
	TIM_OC4PreloadConfig(TIM2, TIM_OCPreload_Enable);
#endif
	TIM_ARRPreloadConfig(TIM2, ENABLE); //使能TIMx在ARR上的预装载寄存器
	TIM_Cmd(TIM2, ENABLE);  //使能TIMx
	TIM_CtrlPWMOutputs(TIM2,ENABLE);  //MOE 主输出使能				 
}

void PWM_TIM3_Init(uint16_t period, uint16_t arr)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;

	RCC_APB2PeriphClockCmd(PWM_TIM3_GPIO_CLK | RCC_APB2Periph_AFIO, ENABLE);  //使能GPIO外设时钟使能
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE); //时钟使能
	
	//设置该引脚为复用输出功能,输出PWM脉冲波形
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;  //复用推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
#ifdef USE_TIM3_CH1
	GPIO_InitStructure.GPIO_Pin = PWM_TIM3_GPIO_PIN;
	GPIO_Init(PWM_TIM3_GPIO_PORT, &GPIO_InitStructure);
#endif
#ifdef USE_TIM3_CH2
	GPIO_InitStructure.GPIO_Pin = PWM_TIM3_GPIO_PIN2;
	GPIO_Init(PWM_TIM3_GPIO_PORT, &GPIO_InitStructure);
#endif
#ifdef USE_TIM3_CH3
	GPIO_InitStructure.GPIO_Pin = PWM_TIM3_GPIO_PIN3;
	GPIO_Init(PWM_TIM3_GPIO_PORT, &GPIO_InitStructure);
#endif
#ifdef USE_TIM3_CH4
	GPIO_InitStructure.GPIO_Pin = PWM_TIM3_GPIO_PIN4;
	GPIO_Init(PWM_TIM3_GPIO_PORT, &GPIO_InitStructure);
#endif
	
	//定时器初始化
	TIM_TimeBaseStructure.TIM_Period = period; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值
	TIM_TimeBaseStructure.TIM_Prescaler = arr; //设置用来作为TIMx时钟频率除数的预分频值
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; //设置时钟分割:TDTS = Tck_tim
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
	TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure); //根据指定的参数初始化TIMx的时间基数单位
 
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2; //选择定时器模式:TIM脉冲宽度调制模式2
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; //比较输出使能
	TIM_OCInitStructure.TIM_Pulse = 0; //设置待装入捕获比较寄存器的脉冲值
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low; //输出极性:TIM输出比较极性高
#ifdef USE_TIM3_CH1
	TIM_OC1Init(TIM3, &TIM_OCInitStructure);  //根据TIM_OCInitStruct中指定的参数初始化外设TIMx
	TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);  	//CH1预装载使能   
#endif	
#ifdef USE_TIM3_CH2
	TIM_OC2Init(TIM3, &TIM_OCInitStructure);
	TIM_OC2PreloadConfig(TIM3, TIM_OCPreload_Enable);
#endif
#ifdef USE_TIM3_CH3
	TIM_OC3Init(TIM3, &TIM_OCInitStructure);
	TIM_OC3PreloadConfig(TIM3, TIM_OCPreload_Enable);
#endif
#ifdef USE_TIM3_CH4
	TIM_OC4Init(TIM3, &TIM_OCInitStructure);
	TIM_OC4PreloadConfig(TIM3, TIM_OCPreload_Enable);
#endif
	TIM_ARRPreloadConfig(TIM3, ENABLE); //使能TIMx在ARR上的预装载寄存器
	TIM_Cmd(TIM3, ENABLE);  //使能TIMx
	TIM_CtrlPWMOutputs(TIM3,ENABLE);  //MOE 主输出使能				 
}

void PWM_setCompare(TIM_TypeDef* TIMx, uint8_t CHx, uint16_t compare)
{
	switch(CHx) {
		case PWM_CH1: TIM_SetCompare1(TIMx, compare); break;
		case PWM_CH2: TIM_SetCompare2(TIMx, compare); break;
		case PWM_CH3: TIM_SetCompare3(TIMx, compare); break;
		case PWM_CH4: TIM_SetCompare4(TIMx, compare); break;
		default: break;
	}
}
