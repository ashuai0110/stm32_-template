#ifndef __USART_AS_H
#define	__USART_AS_H

#include "main.h"

typedef struct {
	void (* USART1_Init)(uint32_t baudRate);
	void (* USART2_Init)(uint32_t baudRate);
	void (* USART3_Init)(uint32_t baudRate);
	void (* USART_SendString1)(USART_TypeDef *USARTx, uint8_t *str, uint16_t len);
	void (* USART_SendString2)(USART_TypeDef* USARTx, uint8_t *str);
} USARTClassStruct;

extern USARTClassStruct USARTClass;

#endif
