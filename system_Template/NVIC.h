#ifndef __NVIC_AS_H_
#define __NVIC_AS_H_

#include "main.h"

typedef struct {
	void (* Init)(void);
} NVICClassStruct;

extern NVICClassStruct NVICClass;

#endif

