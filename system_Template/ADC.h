#ifndef __ADC_AS_H
#define __ADC_AS_H

#include "main.h"

typedef struct {
	void (* Init)(void);
	uint16_t (* GetValue)(uint8_t ADC_channelx);
} ADC1ClassStruct;

extern ADC1ClassStruct ADC1Class;

#endif
