#include "main.h"

static void EXTI_Config(void);

EXTIClassStruct EXTIClass = {
	.Init = EXTI_Config
};

#define EXTI1_PORT		GPIOB
#define EXTI1_CLK			(RCC_APB2Periph_GPIOB)
#define EXTI1_PIN			GPIO_Pin_3
		
static void EXTI_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	EXTI_InitTypeDef EXTI_InitStructure;
	
	RCC_APB2PeriphClockCmd(EXTI1_CLK, ENABLE);
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Pin = EXTI1_PIN;
	GPIO_Init(EXTI1_PORT, &GPIO_InitStructure);

//	// 配置EXTI的信号源
//	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource3);
//	EXTI_InitStructure.EXTI_Line = EXTI_Line3;  // 产生中断的管脚号
//	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;  // 模式 Interrupt中断/Event事件
//	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;  // 中断的触发方式 Rising上升沿/Falling下降沿
//	EXTI_InitStructure.EXTI_LineCmd = ENABLE;  // 使能或失能中断
//	EXTI_Init(&EXTI_InitStructure);
//	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
//	NVIC_InitStructure.NVIC_IRQChannel = EXTI3_IRQn;	
//	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 4;	 
//	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;	
//	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//	NVIC_Init(&NVIC_InitStructure);
}
/* 在别处复现此函数  外部中断函数
void EXTI3_IRQHandler(void)
{
	if (EXTI_GetITStatus(EXTI_Line3) != RESET) {

	}
	EXTI_ClearITPendingBit(EXTI_Line3);
}
*/

