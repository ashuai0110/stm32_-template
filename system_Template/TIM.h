#ifndef __TIM_AS_H
#define __TIM_AS_H

#include "main.h"

typedef struct {
	void (* TIM1_Init)(uint16_t arr, uint16_t psc);
	void (* TIM2_Init)(uint16_t arr, uint16_t psc);
	void (* TIM3_Init)(uint16_t arr, uint16_t psc);
	void (* Overtime_Handle)(uint16_t *counter, uint8_t *overTime_flag, uint16_t overTime, void (* callback)(void));
} TIMClassStruct;

extern TIMClassStruct TIMClass;

#endif
