#ifndef __WDOG_AS_H
#define __WDOG_AS_H

#include "main.h"

typedef struct {
	void (* Init)(void);
	void (* Feed)(void);
} WDOGClassStruct;

extern WDOGClassStruct WDOGClass;

#endif
