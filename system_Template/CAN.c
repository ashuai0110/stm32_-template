#include "can.h"
#include <string.h>

/**
  * @brief  CAN配置
  * @param  
  * @retval None
	* @note		tsjw:重新同步跳跃时间单元.范围:1~3;	CAN_SJW_1tq	CAN_SJW_2tq	CAN_SJW_3tq	CAN_SJW_4tq
			tbs2:时间段2的时间单元.范围:1~8;
			tbs1:时间段1的时间单元.范围:1~16;	  CAN_BS1_1tq ~CAN_BS1_16tq
			brp :波特率分频器.范围:1~1024;(实际要加1,也就是1~1024) tq=(brp)*tpclk1
			注意以上参数任何一个都不能设为0,否则会乱.
			波特率=Fpclk1/((tsjw+tbs1+tbs2)*brp);
			mode:0,普通模式;1,回环模式;
			Fpclk1的时钟在初始化的时候设置为36M,如果设置CAN_Normal_Init(1,8,7,5,1);
			则波特率为:36M/((1+8+7)*5)=450Kbps
  */
u8 CAN_Mode_Init(u8 tsjw,u8 tbs1,u8 tbs2,u16 brp,u8 mode)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	CAN_InitTypeDef CAN_InitStructure;
 	CAN_FilterInitTypeDef CAN_FilterInitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO, ENABLE); //使能PORTA时钟                											 
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE); //使能CAN1时钟
	// CAN_Tx
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	//复用推挽
	GPIO_Init(GPIOA, &GPIO_InitStructure);			//初始化IO
	// CAN_Rx
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;	//上拉输入
	GPIO_Init(GPIOA, &GPIO_InitStructure);			//初始化IO
	
	//CAN单元设置 MCR
 	CAN_InitStructure.CAN_TTCM=DISABLE;	//非时间触发通信模式  //
 	CAN_InitStructure.CAN_ABOM=ENABLE;	//软件自动离线管理	 //
	CAN_InitStructure.CAN_AWUM=DISABLE;	//睡眠模式通过软件唤醒(清除CAN->MCR的SLEEP位)//
	CAN_InitStructure.CAN_NART=ENABLE;	//禁止报文自动传送 //只发一次，不管成功与否
	CAN_InitStructure.CAN_RFLM=DISABLE;	//报文不锁定,新的覆盖旧的 // 
	CAN_InitStructure.CAN_TXFP=DISABLE;	//优先级由报文标识符决定 //
	//设置CAN1波特率
	CAN_InitStructure.CAN_Mode= mode;	//模式设置： mode:0,普通模式;1,回环模式; //
	CAN_InitStructure.CAN_SJW=tsjw;		//重新同步跳跃宽度(Tsjw)为tsjw+1个时间单位  CAN_SJW_1tq	 CAN_SJW_2tq CAN_SJW_3tq CAN_SJW_4tq
	CAN_InitStructure.CAN_BS1=tbs1;	 	//Tbs1=tbs1+1个时间单位CAN_BS1_1tq ~CAN_BS1_16tq
	CAN_InitStructure.CAN_BS2=tbs2;		//Tbs2=tbs2+1个时间单位CAN_BS2_1tq ~	CAN_BS2_8tq
	CAN_InitStructure.CAN_Prescaler=brp;//分频系数(Fdiv)为brp+1	//
	CAN_Init(CAN1, &CAN_InitStructure);
	//配置滤波器组0
	CAN_FilterInitStructure.CAN_FilterNumber=0;	  						//过滤器0 共28组
	CAN_FilterInitStructure.CAN_FilterMode=CAN_FilterMode_IdMask; 		//标志位ID模式 CAN_FilterMode_IdList
	CAN_FilterInitStructure.CAN_FilterScale=CAN_FilterScale_32bit; 		//32位 CAN_FilterScale_16bit
//	CAN_FilterInitStructure.CAN_FilterIdHigh=0x111<<5;							//32位ID //基本ID放入到STID中
//	CAN_FilterInitStructure.CAN_FilterIdLow=0|CAN_ID_STD;	
//	CAN_FilterInitStructure.CAN_FilterMaskIdHigh=0x222<<5;						//32位MASK
//	CAN_FilterInitStructure.CAN_FilterMaskIdLow=0|CAN_ID_STD;
	CAN_FilterInitStructure.CAN_FilterIdHigh=0x0000;							//32位ID //基本ID放入到STID中
	CAN_FilterInitStructure.CAN_FilterIdLow=0x0000;	
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh=0x0000;						//32位MASK
	CAN_FilterInitStructure.CAN_FilterMaskIdLow=0x0000;
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment=CAN_Filter_FIFO0;	//过滤器0关联到FIFO0
	CAN_FilterInitStructure.CAN_FilterActivation=ENABLE; 				//激活过滤器0
	CAN_FilterInit(&CAN_FilterInitStructure);							//滤波器初始化
	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

	NVIC_InitStructure.NVIC_IRQChannel = USB_LP_CAN1_RX0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;     		// 主优先级为1
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;            		// 次优先级为0
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	CAN_ITConfig(CAN1,CAN_IT_FMP0,ENABLE);								//FIFO0消息挂号中断允许
	return 0;
}
u8 canbuff[8];
//CAN1接收中断服务函数			    
void USB_LP_CAN1_RX0_IRQHandler(void)
{
  CanRxMsg RxMessage;
	
  CAN_Receive(CAN1, CAN_FIFO0, &RxMessage);
	
	if(RxMessage.StdId == 0x200)
	{
		memcpy(canbuff,RxMessage.Data,8);
	}
}
//len:数据长度(最大为8)				     
//msg:数据指针,最大为8个字节.
//返回值:0,成功;
//		 其他,失败;
u8 Can_Send_Msg(u32 canid,u8* msg,u8 len)
{	
	u8 mbox;
	u16 i=0;
	CanTxMsg TxMessage;
	TxMessage.StdId=canid;			// 标准标识符为0
	TxMessage.ExtId=0x1012;			// 设置扩展标示符（29位）
	TxMessage.IDE=CAN_Id_Standard;	// 使用扩展标识符
	TxMessage.RTR=CAN_RTR_Data;		// 消息类型为数据帧，一帧8位
	TxMessage.DLC=len;				// 发送两帧信息
	for(i=0;i<len;i++)
	TxMessage.Data[i]=msg[i];		// 第一帧信息          
	mbox= CAN_Transmit(CAN1, &TxMessage);   
	i=0;
	while((CAN_TransmitStatus(CAN1, mbox)!=CAN_TxStatus_Ok)&&(i<0XFFF))i++;	//等待发送结束
	if(i>=0XFFF)return 1;
	return 0;		

}

