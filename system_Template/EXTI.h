#ifndef __EXTI_AS_H
#define __EXTI_AS_H

#include "main.h"

typedef struct {
	void (* Init)(void);
} EXTIClassStruct;

extern EXTIClassStruct EXTIClass;

#endif
