#include "main.h"

static void TIM1_Init(uint16_t arr, uint16_t psc);
static void TIM2_Init(uint16_t arr, uint16_t psc);
static void TIM3_Init(uint16_t arr, uint16_t psc);
static void Overtime_Handle(uint16_t *counter, uint8_t *overTime_flag, uint16_t overTime, void (* callback)(void));

TIMClassStruct TIMClass = {
	.TIM1_Init = TIM1_Init,
	.TIM2_Init = TIM2_Init,
	.TIM3_Init = TIM3_Init,
	.Overtime_Handle = Overtime_Handle
};

//#define use_TIM1
#define use_TIM2
#define use_TIM3

/* 在别的地方复现此函数  TIM1中断函数
void TIM1_UP_IRQHandler(void)
{
	if(TIM_GetITStatus(TIM1, TIM_IT_Update) != RESET) 
	{	
		TIM_ClearITPendingBit(TIM1, TIM_FLAG_Update);
	}
}
*/
/* 在别的地方复现此函数  TIM2中断函数
void TIM2_IRQHandler(void)
{
	if(TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET) 
	{	
		TIM_ClearITPendingBit(TIM2, TIM_FLAG_Update);
	}	
}
*/
/* 在别的地方复现此函数  TIM3中断函数
void TIM3_IRQHandler(void)
{
	if(TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET) 
	{	
		TIM_ClearITPendingBit(TIM3, TIM_FLAG_Update);
	}
}
*/

/**
  * @brief  TIM1初始化
	* @param  None
  * @retval None
  */
static void TIM1_Init(uint16_t arr, uint16_t psc)
{
	#ifdef use_TIM1
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;  
	NVIC_InitTypeDef NVIC_InitStructure; 
  // 设置中断组
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);		
	// 设置中断来源
	NVIC_InitStructure.NVIC_IRQChannel = TIM1_UP_IRQn;	
	// 设置主优先级为 0
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;	 
	// 设置抢占优先级为3
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;	
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);	
	// 自动重装载寄存器的值，累计TIM_Period+1个频率后产生一个更新或者中断
	TIM_TimeBaseStructure.TIM_Period = arr;
	// 时钟预分频数
	TIM_TimeBaseStructure.TIM_Prescaler = psc;	
	// 时钟分频因子 ，没用到不用管
	TIM_TimeBaseStructure.TIM_ClockDivision=TIM_CKD_DIV1;		
	// 计数器计数模式，设置为向上计数
	TIM_TimeBaseStructure.TIM_CounterMode=TIM_CounterMode_Up; 		
	// 重复计数器的值，没用到不用管
	TIM_TimeBaseStructure.TIM_RepetitionCounter=0;	
	// 初始化定时器
	TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);
	// 清除计数器中断标志位
	TIM_ClearFlag(TIM1, TIM_FLAG_Update);
	// 开启计数器中断
	TIM_ITConfig(TIM1,TIM_IT_Update,ENABLE);
	// 使能计数器
	TIM_Cmd(TIM1, ENABLE);
	#endif
}
/**
  * @brief  TIM2初始化
	* @param  None
  * @retval None
  */
void TIM2_Init(uint16_t arr, uint16_t psc)
{
	#ifdef use_TIM2
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;  
	NVIC_InitTypeDef NVIC_InitStructure; 
  // 设置中断组
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);		
	// 设置中断来源
	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;	
	// 设置主优先级为 0
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;	 
	// 设置抢占优先级为3
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;	
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);	
	// 自动重装载寄存器的值，累计TIM_Period+1个频率后产生一个更新或者中断
	TIM_TimeBaseStructure.TIM_Period = arr;
	// 时钟预分频数
	TIM_TimeBaseStructure.TIM_Prescaler= psc;	
	// 时钟分频因子 ，没用到不用管
	TIM_TimeBaseStructure.TIM_ClockDivision=TIM_CKD_DIV1;		
	// 计数器计数模式，设置为向上计数
	TIM_TimeBaseStructure.TIM_CounterMode=TIM_CounterMode_Up; 		
	// 重复计数器的值，没用到不用管
	TIM_TimeBaseStructure.TIM_RepetitionCounter=0;	
	// 初始化定时器
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
	// 清除计数器中断标志位
	TIM_ClearFlag(TIM2, TIM_FLAG_Update);
	// 开启计数器中断
	TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);
	// 使能计数器
	TIM_Cmd(TIM2, ENABLE);
	#endif
}
/**
  * @brief  TIM3初始化
	* @param  None
  * @retval None
  */
void TIM3_Init(uint16_t arr, uint16_t psc)
{
	#ifdef use_TIM3
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;  
	NVIC_InitTypeDef NVIC_InitStructure; 
  // 设置中断组
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);		
	// 设置中断来源
	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;	
	// 设置主优先级为 0
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;	 
	// 设置抢占优先级为3
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;	
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);	
	// 自动重装载寄存器的值，累计TIM_Period+1个频率后产生一个更新或者中断
	TIM_TimeBaseStructure.TIM_Period = arr;
	// 时钟预分频数
	TIM_TimeBaseStructure.TIM_Prescaler = psc;	
	// 时钟分频因子 ，没用到不用管
	TIM_TimeBaseStructure.TIM_ClockDivision=TIM_CKD_DIV1;		
	// 计数器计数模式，设置为向上计数
	TIM_TimeBaseStructure.TIM_CounterMode=TIM_CounterMode_Up; 		
	// 重复计数器的值，没用到不用管
	TIM_TimeBaseStructure.TIM_RepetitionCounter=0;	
	// 初始化定时器
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);
	// 清除计数器中断标志位
	TIM_ClearFlag(TIM3, TIM_FLAG_Update);
	// 开启计数器中断
	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);
	// 使能计数器
	TIM_Cmd(TIM3, ENABLE);
	#endif
}
/**
  * @brief  定时器非堵塞延时处理函数
  * @param  counter: 对应计数器地址
  * @param  overTime_flag: 对应超时标志地址
  * @param  overTime: 超时时间
  * @param  callback: 超时结束的回调函数
  * @retval None
  * @note	None
  */
static void Overtime_Handle(uint16_t *counter, uint8_t *overTime_flag, uint16_t overTime, void (* callback)(void))
{
	if(*overTime_flag)
	{
		*counter = *counter + 1;
		if((*counter >= overTime))
		{
			*overTime_flag = 0;
			*counter = 0;
			callback();
		}
	}

}

