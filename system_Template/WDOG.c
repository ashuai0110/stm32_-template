#include "main.h"

static void WDOG_Init(void);
static void WDOG_Feed(void);

WDOGClassStruct WDOGClass = {
	.Init = WDOG_Init,
	.Feed = WDOG_Feed
};

/**
  * @brief  看门狗初始化
  * @param  None
  * @retval None
  */
static void WDOG_Init(void)
{
	// 使能 预分频寄存器PR和重装载寄存器RLR可写
	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
	// 设置预分频器值
	IWDG_SetPrescaler(IWDG_Prescaler_16); // IWDG 1s 超时溢出40k/16/2500= 1s
	// 设置重装载寄存器值
	IWDG_SetReload(0xfff);
	// 把重装载寄存器的值放到计数器中
	IWDG_ReloadCounter();
	// 使能 IWDG
	IWDG_Enable();    
}
/**
  * @brief  重置看门狗
  * @param  None
  * @retval None
  */
static void WDOG_Feed(void)
{
	// 把重装载寄存器的值放到计数器中，喂狗，防止IWDG复位
	// 当计数器的值减到0的时候会产生系统复位
	IWDG_ReloadCounter();
}



